#!/bin/bash
####
# Ce script deploie ../main.pdf vers ../../electrocinetique.pdf en gardant archivant la précedante
### 

pushd `dirname $0` > /dev/null
SCRIPTPATH=`pwd -P`
popd > /dev/null


TIMESTAMP=`date +%Y_%m_%d_%H_%M_%S`
echo "TIMESTAMP is $TIMESTAMP"
cd $SCRIPTPATH

cp "../main.pdf" "../exports/electrocinetique_$TIMESTAMP.pdf" 
cp "../exports/electrocinetique_$TIMESTAMP.pdf" "../../electrocinetique.pdf"
