#!/bin/bash
####
# Ce script sert à exporter tous les fichier markdown dans le dossier markdown vers du latex dans /build
# requiert pandoc
###
pushd `dirname $0` > /dev/null
SCRIPTPATH=`pwd -P`
popd > /dev/null

ROOTDIR=`dirname $SCRIPTPATH`
echo "ROOT dir = $ROOTDIR"

for i in "$ROOTDIR"/src/md/*.md; do
 	[ -f "$i" ] || break
 	echo "processing : $i "
	FILENAME=$(basename -- "$i")
	FILENAME="${FILENAME%.*}"
	echo $FILENAME
	pandoc "$i" -o "$ROOTDIR/build/tex/$FILENAME.tex"
done
