#!/bin/bash
####
# Ce script sert à exporter la bibliographie depuis Zotero
# Requiert que Zotero fonctionne 
# Requiert le plugin betterbibtex actif
####
pushd `dirname $0` > /dev/null
SCRIPTPATH=`pwd -P`
popd > /dev/null

COLLECTION="reinjection_2018"
BIBTEX_URL="http://localhost:23119/better-bibtex/collection?/1/$COLLECTION.bibtex"
BIBLATEX_URL="http://localhost:23119/better-bibtex/collection?/1/$COLLECTION.biblatex"

echo "running $0"

ROOTDIR=`dirname $SCRIPTPATH`
#echo "ROOT dir = $ROOTDIR"

echo check if "$BIBTEX_URL" exit

if curl --output /dev/null --silent --head --fail "$BIBTEX_URL"; then
  printf '%s\n' "$BIBTEX_URL exist, fetching bibtex to references.bib"
  curl $BIBTEX_URL > $ROOTDIR/build/bib/bib.bib
else
  printf '%s\n' "$BIBTEX_URL does not exist, not exporting bibtex"
fi


echo check if "$BIBLATEX_URL" exit

if curl --output /dev/null --silent --head --fail "$BIBLATEX_URL"; then
  printf '%s\n' "$BIBLATEX_URL exist, fetching biblatex to references.bib"
  curl $BIBLATEX_URL > $ROOTDIR/build/bib/biblatex.bib
else
  printf '%s\n' "$BIBLATEX_URL does not exist, not exporting biblatex"
fi



echo "finish $0"
