#!/bin/bash
####
# Ce script sert à exporter tous les fichier markdown dans le dossier markdown vers du latex dans /build
# requiert pandoc
###
pushd `dirname $0` > /dev/null
SCRIPTPATH=`pwd -P`
popd > /dev/null

ROOTDIR=`dirname $SCRIPTPATH`
echo "ROOT dir = $ROOTDIR"
cd "$ROOTDIR/src/md/"

cat *.md >> ../all.md

