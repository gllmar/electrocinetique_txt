\chapter*{remerciement}

Un remerciement particulier à André Éric Létourneau, Gisèle Trudel et Alexandre Castonguay pour leurs précieux conseils, nos conversations et les pistes de recherche qu’ils m’ont suggérées. Un grand merci à Alexandre Burton et Line Nault qui m’ont partagé leur rapport à la création artistique dans de nombreux contextes. À Viva Paci et Maïa Arseneault qui ont su canaliser en moi les fondements de la vie. À mes parents ainsi qu'à Marie-Pierre Arseneault et Philippe Messier qui m’ont épaulé en fin de course, à mes collègues, amies et amis qui m’ont accompagné à travers ce long processus. 

