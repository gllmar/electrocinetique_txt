\renewcommand{\appendixname}{appendice}
\appendice


\chapter{Ars Electronica 2018}

Suite au dépôt du mémoire, *Électrocinétique* fut présenté à Ars Electronica 2018. *Feedback Cycles for Oscillographes* fut choisi comme nom d'œuvre à l'international.

\begin{figure}
\centering
\caption{Extrait du site web \citep{hexagram_campus_hexagram_2018}}
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/03_ars/hexa/ae_campus_taking_care}
\label{fig:arsHexa}
\end{figure}


\begin{figure}
\centering
\caption{Extrait du catalogue \citep[p.227]{ars_electronica_ars_2018} }
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/03_ars/catalogue/extrait_catalogue}
\label{fig:arsCatalogue}
\end{figure}


\begin{figure}
\centering
\caption{Extrait du programme court \citep[p.44]{ars_electronica_timetable_2018} }
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/03_ars/catalogue/extrait_programme_court}
\label{fig:arsProgram}
\end{figure}


\begin{figure}
\centering
\caption{Performance Music Monday \citeyearpar{ars_electronica_music_2018} }
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/03_ars/selection/ars_1}
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/03_ars/selection/ars_2}
\label{fig:ars1}
\end{figure}

\begin{figure}
\centering
\caption{Performance Music Monday suite \citeyearpar{ars_electronica_music_2018}}
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/03_ars/selection/ars_3}
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/03_ars/selection/ars_4}
\label{fig:ars2}
\end{figure}


\begin{figure}
\centering
\caption{Rencontre avec Roy Macdonald}
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/03_ars/selection/rencontre_avec_Roy}
\label{fig:arsRoy}
\end{figure}



\begin{figure}
\centering
\caption{Branchements}
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/03_ars/schema/oscillosaltzam}
\label{fig:arsBranchements}
\end{figure}

\chapter{Documents d'appel}

Suite à la présentation d'*Électrocinétique* à *Ars Electronica*, une étape de rédaction/condensation permit de cadrer l'oeuvre dans des cases de formulaires d'appel de dossier. 

\newpage

**Description** 

*Électrocinétique* est une installe-action où un vélo est transformé en instrument sonore. Le mouvement exécuté sur son pédalier produit l'énergie nécessaire afin d'alimenter un mélangeur audio qui réinjecte ses signaux de sortie dans ses entrées. Le registre expressif de cet instrument volontairement instable est proportionnel à l'énergie résultant de l'effort fourni. 
Sur les murs de la pièce est projetée l'analyse du signal audio produit depuis une multitude de projecteurs vidéo. Les particularités sonores sont alors visuellement révélées. En résulte une synesthésie audiovisuelle hypnotique de lignes qui se tracent en fonction de la disparité stéréophonique du son que l'on entend. 
Le balai de formes et de sons est activé quotidiennement par le performeur et laissé disponible le reste du temps aux curieuses et curieux qui voudrait révéler l’intangible situé entre la discontinuité et le court-circuit.


\newpage

**Devis technique **

**Audio**

* 4 Haut parleurs actifs 
	* QSC k8.2 ou relativement équivalent en termes de réponse/présence
	* seront placé au sol tel que des moniteurs 
	* si haut-parleurs passif, autant de canaux d'amplification
* 1 Subwooffer actif  
* Un mélangeur audio disposant d'au moins 4 entrées et de 4 sorties (moniteur+main mix) 
* Filage approprié pour couvrir les distances entre le mélangeur en régie et les haut-parleurs.  


**Vidéo**

* 2 projecteurs vidéo récents HD
* ~5 projecteurs vidéo moins récent  
	* la diversité des projecteurs, leur âge et leur défaut sont des qualités intrinsèques au projet.
	* doivent être dotés d'une entrée vidéo analogue (composite)
* Câble d'alimentation pour chacun des projecteurs

**Électricité**

* 2 circuits électriques 15 Ampère 120v 
	* 1 dédié à l'audio
	* ~1 pour la vidéo (relatif au wattage total des projecteurs)
* Lot de filage/extension AC (U-ground) et multiprises

**Scénographique**

* 1 vélo de grandeur standard, mécanique fonctionnelle 
* 1 structure capable de tenir le vélo lorsqu'il est à l'envers sur ce dernier 
	* l'objectif ici est que la pédale soit à hauteur accessible 



\chapter{Outils de publication du mémoire}

Le présent document utilise l'outil de composition \hologo{LaTeX} \citep{latex3_team_latex_1983} et la classe memoireuqam1.3 \citep{legault_classe_2018}

La source du présent document \citep{arseneault_electrocinetique_2018} est hébergée à l'URL suivant :

* \url{https://gitlab.com/gllmar/electrocinetique_txt/}

La documentation audiovisuelle de l'œuvre est rassemblée à l'URL suivant : 

* \url{http://gllm.ca/projets/electrocinetique/}
