\chapter*{remerciement}

Un remerciement particulier à André Éric Létourneau, Gisèle Trudel et Alexandre Castonguay pour leurs précieux conseils, nos conversations et les pistes de recherche qu’ils m’ont suggérées. Un grand merci à Alexandre Burton et Line Nault qui m’ont partagé leur rapport à la création artistique dans de nombreux contextes. À Viva Paci et Maïa Arseneault qui ont su canaliser en moi les fondements de la vie. À Marie-Pierre Arseneault et Philippe Messier qui m’ont épaulé en fin de course ainsi qu’à mes collègues, amies et amis qui m’ont accompagné à travers ce long processus. 

\abstract{}

À partir des années 1950 et dans la lignée des penseurs comme Norbert Wiener ou Gilbert Simondon, une pensée de l’objet technique et des modes de communication se développe autant dans des approches matérielles que philosophiques. Ce mémoire de recherche-création s’inscrit dans le sillon des réflexions de ces penseurs et cherche plus spécifiquement à situer une recherche-création issue du principe de concrétisation de l'objet technique appliqué à la création d'une installation interactive. L'œuvre instrument issue du processus dévoile la rétroaction entre les éléments d’un système en boucle ouverte. L'action technique sur la mécanique d'un vélo sert d’interface à une expérience où sons et lumières vidéo émergent de manipulations sensibles. Le projet est une tentative de concrétisation de l’objet technique visant à créer une machine autonome où chaque composante interagit avec son milieu. Le processus de recherche-création qui s’est développé autour de cette idée sera présenté ici mettant en évidence les progrès et les errances de son développement.

 
MOTS-CLÉS: \myKeywords
\introduction{}

# Origine, intérêt et intentions

Depuis plusieurs années, je me passionne pour la création d’installations audiovisuelles produisant des expériences à partir de l'interaction entre le mouvement corporel sur un mécanisme doté de senseurs électroniques et de la programmation logicielle. 
*B-cycle* \citeyearpar{arseneault_b-cycle_2012}, 
*Pédalier à image* \citeyearpar{arseneault_pedalier_2013},
*RUSH* \citeyearpar{arseneault_rush_2014}, 
*Les Marcels* \citeyearpar{arseneault_les_2014}, 
*Arbol* \citeyearpar{arseneault_arbol_2015}, 
*Veloce* \citeyearpar{arseneault_veloce_2016} 
sont autant de projets qui impliquent le mouvement d’un corps en relation avec la mécanique de vélos en échange d’une expérience audiovisuelle. Tous ces projets m’ont amené à réfléchir aux technologies que j’emploie pour créer ces moments d'expérimentation et ces espaces d'interactions.  

Ces assemblages complexes de technologies, de logiciels et de matériaux tendent à s'inspirer du principe d'instrument de musique qui interagit avec ceux qui le jouent. Je choisis de nommer cet ensemble un instrument interactif. Les instruments interactifs interagissent avec les mouvements du spectateur-usager par la production de lumière, de sons ou de projections vidéo.

J'utilise des composantes de vélo comme interfaces accessibles aux interacteurs\footnotemark, afin de les inciter à fournir un mouvement familier comme celui de pédaler ou de faire tourner une roue. Au gré des projets, j’ai développé du matériel électronique qui sert à capter le mouvement lors de son activation par un être humain. Ces composantes électroniques communiquent avec des logiciels que je programme et qui servent à transformer l'action et ainsi révéler le comportement de l'instrument. Le matériel électronique transforme un mouvement senti en flux d'information numérique. Ce flux de données alimente une programmation logicielle qui s'exprime en sons audibles, en réactions lumineuses et en images vidéo projetées. 

\footnotetext
{
J’utilise le terme interacteur pour décrire à la fois l’opérateur technicien \citep{simondon_du_1958} qui utilise la machine avec une connaissance de son comportement ainsi que le spectateur-usager curieux qui le découvre. La différence entre ces deux types d'interacteur est la compréhension préalable du fonctionnement de la boîte de noire \citep{latour_science_1987}. Ce qui les relie est leurs manipulations de l'interface qui permet à la machine de révéler son contenu \citep{paquin_comprendre_2006}.
}

Avec du recul, chacun de ces projets constitue une étape importante dans la concrétisation d’*Électrocinétique*. Mon intérêt à comprendre le matériel que j’utilise m’a amené à utiliser moins de matériel, mais à mieux le maîtriser. Alors qu’avec *B-Cycle* (2012), j’avais besoin de deux ordinateurs récents, trois projecteurs vidéo, une équipe de cinq personnes, un outillage lourd ainsi qu’une programmation complexe et fragile, en 2016, je présentais *Véloce* avec beaucoup moins de matériel (un ordinateur portable et un projecteur), et ce en étant auteur de l’entièreté du code. Outre la technologie informatique qui devient de plus en plus performante et miniaturisée, c’est surtout ma maîtrise progressive des matériaux et de la programmation qui m’a amené à être plus agile, conscient et autonome dans mes déploiements. *Électrocinétique* s’inscrit en continuité de cette démarche; mieux comprendre pour réduire l'empreinte et ainsi optimiser le processus afin de révéler un système plus efficace.

# Formulation du problème et ancrage théorique.

L’objet de ma recherche-création intitulé *Électrocinétique : réinjection cinétique interactive* consiste à créer une pièce audiovisuelle à partir d’un dispositif matériel et d’une programmation logicielle qui tendent vers l’autonomie électrique tout en demeurant les plus expressifs possible, à la manière d’un instrument de musique.
 
En créant *Électrocinétique*, je cherche à faire cheminer ma réflexion critique autour de la technique. Entre alors en jeu l’idée d’empreinte technologique qui correspond à l’ensemble des ressources matérielles, humaines et électriques nécessaires au fonctionnement de l’installation. Ma méthodologie de recherche et de création ainsi que ma maîtrise des outils permettent de créer des instruments interactifs qui tendent, avant tout, vers la réduction de cette empreinte technologique. Le défi réside alors dans un équilibre entre cette intention et la volonté de produire une installation ayant un fort potentiel expressif d’un point de vue artistique. L’instrument doit ainsi être capable de produire une variété de sons et d’images suffisante pour que l’interacteur puisse produire des œuvres sans ressentir, dans son utilisation, les contraintes techniques auxquelles a répondu son développement.

Deux concepts sont notamment au cœur de cette recherche-création, le premier est celui de la *rétroaction*, issue de l’approche cybernétique \citep{wiener_cybernetics_1948} qu’on définira ici rapidement, pour y revenir plus tard, comme le phénomène par lequel, au sein d’un système l’action d’un élément sur un autre entrainera une réponse du second sur le premier. Le second concept est le principe de *concrétisation de l’objet technique* \citep{simondon_du_1958}. 

Ce qui nous intéresse dans cette idée est la réflexion autour de la matérialisation sous forme d’objet ( dispositif ou instrument interactif) de notre idée technique initiale par un processus de condensation des fonctions des différents éléments : « l’invention apporte une vague de condensations, de concrétisations qui simplifient l’objet en chargeant chaque structure d’une pluralité de fonctions» \citep{simondon_imagination_2008}. Simondon explique ce processus en invoquant le changement des composantes servant à dissiper la chaleur issue du fonctionnement d'un moteur à combustion.

>Ces ailettes de refroidissement, dans les premiers moteurs, sont comme ajoutées de l’extérieur au cylindre et à la culasse théoriques, géométriquement cylindriques ; elles ne remplissent qu’une seule fonction, celle du refroidissement. Dans les moteurs récents, ces ailettes jouent en plus un rôle mécanique, s’opposant comme des nervures à une déformation de la culasse sous la poussée des gaz \citep{simondon_du_1958}

Cet exemple de concrétisation donne la direction de la méthodologie de recherche-création que je mets sur pied. La réduction du nombre de composantes et l'optimisation de leur rôle au sein de l'objet constituent un axe structurant de ma démarche. Je propose donc de réfléchir autour d'une problématique à laquelle je suis confronté lorsque je crée des dispositifs\footnotemark  en utilisant des technologies : comment concrétiser l’empreinte technologique et énergétique d’un instrument interactif dont l'interaction révèle un ensemble de rétroactions audiovisuelles? Cette recherche-création se structure autour de trois axes soient:

1. L'adoption d'une méthode de recherche-création cherchant à tendre vers la concrétisation de l'objet technique.
1. Le développement et la maîtrise d’un environnement de création permettant la réduction des ressources nécessaires pour produire l'instrument audiovisuel interactif. 
1. La présentation sous forme performative d’une pièce sur l’instrument interactif cré et où le registre expressif maximise la transformation de mouvements en sons et que ces sons  deviennent des images en temps réel.
\enlargethispage{\baselineskip} 
\footnotetext
{
Bien que Michel Foucault ait défini le dispositif à travers une perspective poststructuraliste par entre autres l’exemple du panoptique \citeyearpar{foucault_surveiller_1975} je ne me base pas ici sur sa définition. Le cas actuel décrit plutôt le dispositif comme étant un ensemble de composantes comme l’interface, le matériel, son comportement, son contenu, son contexte. 
}

# Premier aperçu du but du mémoire

*Électrocinétique* est le nom de la pièce alors qu'*Oscillographe* est le nom du logiciel régissant le comportement de l’instrument interactif, l’instrument interactif est l’assemblage formé par l’interface, les logiciels et le matériel permettant à Électrocinétique d’être vu et entendu et de produire des expériences en cocréation avec l'interacteur. 

La pièce *Électrocinétique, réinjection cinétique interactive* s’inscrit dans une démarche de réduction d’empreinte qui implique une maitrise du matériel et du logiciel. L’œuvre a été conçue en cherchant à minimiser l’énergie électrique nécessaire à un déploiement de performance audiovisuelle et en misant sur le développement de composantes très peu énergivores alimentées grâce à des batteries rechargeables. L’objectif était de rendre l’instrument interactif autonome au niveau électrique et suffisamment portatif pour être transporté dans un sac à dos et déployé de manière impromptue. 
À travers ce processus de recherche-création, je mobilise l’objet familier du vélo afin de le détourner pour produire une expérience augmentée. Aussi, l’expérience créée est de nature nomade et éphémère, l’instrument est mon vélo et mon moyen de transport. L’objectif est alors d’offrir les possibilités de réalisation de la scène suivante : 

*Sur une place de nuit, un cycliste s’arrête devant les passants. Après avoir retourné son vélo, il sort de son sac à dos un dispositif qu’il installe en quelques minutes sans se brancher à quoi que ce soit d’autre que le véhicule qu’il chevauchait encore il y a quelques minutes. Certains passants s’arrêtent intrigués. Le jeune homme se met alors à jouer avec le pédalier et à modifier la vitesse de rotation des roues. Devant une foule de plus en plus nombreuse, des images prennent forment projetées contre le mur qui borde la place tandis qu’une mélodie électronique emplie l’espace d’une étrange composition musicale interpellant quiconque veut bien ouvrir l’oreille. Après quelques minutes de cet étrange spectacle, le cycliste range son appareil dans son sac à dos, en moins de temps encore qu’il n’en a mis pour l’installer. Il remonte sur son vélo puis s’en va, sans laisser de trace physique.*

Le son d’*Électrocinétique* est issu de rétroaction (*feedbacks*) dans une console de mixage audio. Pour produire cet état fragile sur la console, je branche la sortie gauche et droite du mélangeur dans les entrées gauche et droite correspondantes. En changeant l'amplification et en manipulant les filtres de fréquences intégrés, je crée le son tout en me laissant surprendre par son caractère quelque peu imprévisible. J'enregistre des séances de manipulations d'où j'isole certains segments audio que je juge pertinents créant ainsi des échantillons qui feront partie de la bibliothèque sonore de l’instrument.

Ces échantillons sonores sont joués dans le lecteur d’échantillons d’Oscillographe. Ce lecteur d’échantillons a été programmé de façon à pouvoir moduler la vitesse de lecture du son proportionnellement au mouvement d’une roue de vélo. Cela a pour effet de pouvoir influencer la hauteur du son tel un lecteur vinyle où l’on contrôlerait la vitesse de rotation du disque. Plus il y a du mouvement, plus le son joue rapidement. C’est une de ces variables qui est jouée devant public. 

L’approche visuelle que je développe est en lien direct avec les outils qui permettent de comprendre les signaux électriques. La dimension visuelle d’Oscillographe est un oscilloscope qui compare l’amplitude du signal sonore stéréo. La transformation du signal audio vers un signal vidéo, effectuée par Oscillographe, est calquée sur les oscilloscopes analogiques, plus particulièrement ceux qui permettent de comparer deux signaux (X-Y) et qui peuvent servir de moniteur vectoriel. Contrairement aux oscilloscopes X-Y conventionnels qui traduisent le son en image sur un plan bidimensionnel, Oscillographe est construit dans un moteur de rendu tridimensionnel et maintient la trace temporelle du signal dans une forme 3d dynamique tout en maintenant le principe fondamental de comparaison de signaux sur deux axes.

Les échantillons bruts, visualisés dans Oscillographe, créent un ballet de lignes directement influencé par ce que l’on entend. La circulation des signaux en temps réel, leur nature, leur organisation, leur phase et leur chaos sont les matières premières d’Électrocinétique, le mouvement qui active le dispositif. 

À l’issue de cette première approche, nous avons une idée initiale du projet et des questions qu’il soulève. Pour en rendre compte, nous développerons d’abord les questions introduites par Simondon et Wiener concernant l’objet technique, son évolution, son fonctionnement et les paramètres internes de celui-ci dans le contexte d’un dispositif de communication avec une entrée et une sortie. Pour donner suite à cette réflexion technique, nous discuterons des implications esthétiques et culturelles d’un tel travail. Il nous restera ensuite à expliquer en détail l’élaboration de l’œuvre avant de faire un retour sur la performance autant en termes de réception des spectateurs que dans une initiative d’autocritique.

\chapter{Ancrages conceptuels}
 
 
Si c’est avant tout ma pratique qui me confronte à différentes problématiques d’électronique, de programmation, de consommation de ressources et d’énergie, développer *Électrocinétique* dans un cadre universitaire m’a poussé à me demander si les problèmes que je rencontre dans le contexte actuel et avec des technologies sont nouveaux ou s’ils trouvent racines dans des réflexions technologiques et médiatiques antérieures. Afin de dépasser un enjeu purement pratique qui réduirait l’œuvre à elle-même, et pour que les questions que je souhaite soulever trouvent un écho plus large en tant qu’acte de recherche-création dans une histoire des communications des instruments interactifs, il est nécessaire d’inscrire mon propos dans la continuité de quelques penseurs importants tels que Gilbert Simondon et Norbert Wiener.


# Ancrage dans la contemporanéité

Dans cette section, j'aborderai le concept de boucle de rétroaction emprunté à la théorie de la cybernétique et j’expliquerai comment il s’inscrit dans mon processus de recherche-création. J’expliquerai également l’influence qu’a eue le principe de la concrétisation de l'objet technique de Gilbert Simondon sur le développement de ma méthodologie de recherche-création. 


## Cybernétique et boucle de rétroaction

Cette section se concentre sur la boucle de rétroaction qui est centrale dans Électrocinétique. Il s’agit d’un principe fondamental de la théorie cybernétique de Norbert Wiener \citeyearpar{wiener_cybernetics_1948}. La boucle de rétroaction est définie comme telle :« the behavior of an object is controlled by the margin of error at which the object stands at a given time with reference to a relatively specific goal» \footnotemark \citep[p. 19]{rosenblueth_behavior_1943}. Entendons par là, qu’au sein d’un système, les éléments agissent réciproquement les uns sur les autres : l’action d’un élément A sur un élément B entraînera une réaction de B sur A qui aura une répercussion dans l’action initiale de A sur B. Dès lors, l’action est affectée par cette interférence qui est à l’origine de la marge d’erreur.

\footnotetext{
«comportement de l'objet affecté par la marge d’erreur qui le sépare de l’objectif qu’il cherche à atteindre au temps donné» [notre traduction]
}

Ce principe s’applique à plusieurs étapes de traitement d’Électrocinétique ainsi qu’à presque tous mes projets précédents. L’idée générale reliant mes créations est que l’usager fournit un mouvement pour mettre en fonction le système. Alors que le système tend vers le repos, le mouvement agit comme la marge d’erreur, augmentant l’écart au repos pour le système. 

Si l’on considère l'instrument comme une boucle de réaction au repos qui fonctionne sans intervention extérieure, le mouvement agit comme du bruit et modifie la boucle de rétroaction. Le bruit influence la marge d’erreur et altère la chaîne de transmission de l'information du système. Une fois le mouvement acquis par un système de capteurs (senti), la quantité de mouvement dégagée par l’usage augmente la valeur d’une variable. Cette relation se traduit numériquement par une incrémentation, soit l’ajout de 1 à la valeur interprété par le logiciel. Afin que cette valeur ne dépasse pas un maximum établi, elle décroit naturellement avec le temps et se stabilise à son minimum.
 
De manière plus imagée, c’est comme si l’on tentait de remplir un récipient d’eau ayant une fuite. Ce récipient tend naturellement à se vider, mais, lorsqu’on le remplit, il accumule quand même temporairement de l’eau. Dans le cas d’*Électrocinétique*, la quantité d’eau présente dans le récipient peut se transposer en une variable que l’on peut appeler activité. Le concept d’*activité* est lié à l’action de fournir du mouvement. Cette rétribution à l’action s’accumule proportionnellement au mouvement fourni et met le système en marche, notamment en activant la lecture d’échantillons sonores.

Le principe de la boucle de rétroaction se manifeste lors de la captation d'échantillons. L’ensemble des sons entendus dans la pièce *Électrocinétique* sont issus de *feedbacks* de console. Le son qui émerge de ce processus est une boucle de rétroaction continue et infinie qui génère le signal. À partir du bruit résiduel dans la console, par amplification et par filtrage il émerge un signal brut qui constitue la base sonore d’*Électrocinétique*. En effet, une partie du son n’est pas véritablement généré au moment de l’usage de l’instrument, c’est cette boucle préenregistrée qui va constituer la gamme de l’instrument. L’utilisateur influe sur la lecture de cette boucle.

De manière plus large, le principe de boucle de rétroaction est le fil conducteur qui structure l’ensemble de mes créations. Ce que je cherche fondamentalement est de créer et présenter des dispositifs qui incitent à fournir du mouvement sur un système qui tend au repos. Ainsi, le mouvement devient en quelque sorte une perturbation dans le comportement de la machine, mettant ainsi en relation, voire même en interaction, l’humain et le système. L’idée est alors de rendre le plus efficace possible ce système simple, la boucle est une première étape qui multiplie les fonctions de chacun des éléments du système et laisse entrevoir l’idée qui nous intéressera ensuite : la concrétisation de l’objet technique.

## Concrétisation de l’objet technique

J’aborde ici comment le principe de la concrétisation de l’objet technique influence mon processus de recherche-création. Ce qui m’amène à créer *Électrocinétique* est avant tout une volonté de compréhension de mon rapport à la technique. Je cherche à comprendre le fonctionnement des objets, à accroître leur efficience, et ce en élaguant ce qui devient inutile à force de mieux comprendre leur fonctionnement. C’est cette quête d’optimisation qui m’amène à m’intéresser aux propos de Gilbert Simondon et plus précisément au concept de concrétisation de l’objet technique. Des liens peuvent être établis entre ma démarche de créateur et le passage suivant :

> C’est à cause de cette recherche des synergies que la concrétisation de l’objet technique peut se traduire par un aspect de simplification; l’objet technique concret est celui qui n’est plus en lutte avec lui-même, celui dans lequel aucun effet secondaire ne nuit au fonctionnement de l’ensemble ou n’est laissé en dehors de ce fonctionnement \citep[p.34]{simondon_du_1958}.  

La concrétisation est fondamentale dans mon cursus et m’intéresse depuis plusieurs années. Ma création d’instruments interactifs est motivée par la volonté d’élaguer le plus possible ce qui semble lutter contre le processus de co-création de l’objet. Par exemple, ce phénomène s’exprime lorsqu’un outil tel qu’un système d’exploitation générique est utilisé pour exécuter une fonction très précise comme celui d’être un instrument interactif. Le système d’exploitation générique n’est pas nécessairement informé de la précision de son opération et pourrait à tout moment décider qu’il désire se mettre à jour et l’afficher à l’écran, brisant ainsi l’expérience que le système est sensé produire d’où la nécessité de développer des objets spécialisés qui répondent spécifiquement à la tâche qui leur incombe.

C’est de concert avec la concrétisation de l’objet technique que je cherche à développer une méthode pour créer librement des instruments interactifs en priorisant la capacité d'approfondir le comportement d'un maximum de composantes que je mets en relation. Cette démarche m’amène à comprendre les constituantes techniques, matérielles et logicielles afin que je puisse les réduire à l'essentiel. Ce vecteur de recherche-création se manifeste particulièrement dans le rapport qu’entretient la pièce *Électrocinétique* à l'électricité que son opération requiert. Le fait de tendre vers l’autonomie énergétique dans cette pièce sous-tend ma réflexion sur la réduction de consommation électrique qu’implique l'usage d'un instrument interactif.  En cherchant à réduire l’empreinte énergétique d’*Électrocinétique* cela implique le développement d’un système très optimisé et peu énergivore afin de pouvoir diffuser l’œuvre.  

L'exemple de la concrétisation du générateur et turbine documenté par \cite{simondon_du_1958} a beaucoup inspiré ma recherche-création. La turbine de Guimbal (voir figure \ref{fig:guimbal_1} \citep{guimbal_combined_1953}) est utilisée dans les barrages hydroélectriques et se distingue des générateurs électriques contemporains par son mode de refroidissement. Cette turbine est un exemple du processus de concrétisation, car elle permet, entre autres choses, d’utiliser l’eau de manière plurifonctionnelle : l’eau fournit l’énergie, mais sert aussi de système de refroidissement et de stabilisation de la turbine. Cela a pour effet de rendre l'objet très efficace dans sa fonction tout en nécessitant moins de matériel pour son opération.  Cette relation plus efficace entre son usage (générer de l’énergie), son environnement (immergé) et son fonctionnement (entrées d’eau et sortie d’énergie) n’est pas visible de l’extérieur, mais cette dynamique d’un ensemble de propriétés et de fonctions qui se condensent et qui se simplifient représente la concrétisation. 

Cette démarche de concrétisation implique que le développement tend vers l'infini sans toutefois complètement se réaliser.  *Électrocinétique* tends vers la concrétisation autant qu’il tend vers l’autonomie énergétique. Cette tension poursuit un autre caractère de la concrétisation de l’objet technique, le manque d'indétermination : 

> Il subsiste une certaine distance entre le système des intentions techniques correspondant à une finalité définie et le système scientifique de la connaissance des interactions causales qui réalisent cette fin; l’objet technique n’est jamais complètement connu; pour cette raison même, il n’est jamais non plus complètement concret \citep{simondon_du_1958}.

*Électrocinétique* ne sera jamais parfaitement concrétisé étant donné qu'il demeurera toujours un peu d'inconnu, mais la quête de la concrétisation de l'objet technique est la fondation de ma démarche de créateur d’instruments interactifs. 

# Ancrage dans les concepts structurant l’œuvre

## Schématisation 

La schématisation est une phase de planification graphique grandement inspirée du modèle de communication mathématique \citep{shannon_mathematical_1948} elle-même inspirée des principes de la théorie cybernétique \citep{wiener_cybernetics_1948}. Dans l’idée de visualiser une situation de communication où interviennent différents éléments, dans notre cas digital et physique, on construit un schéma simplifié permettant de mettre en évidence les différents éléments et la manière avec laquelle ils sont censés interagir.
On construit sur papier la relation entre les différentes composantes agissant sur un système sous forme de diagrammes.  Ils ne présupposent pas de solutions ou de méthodes de réalisation, ils se concentrent sur les processus de relations qui définissent les rapports structurant les objets et concepts façonnant *Électrocinétique*. 

Ces schémas présentés en annexe constituent une projection du fonctionnement escomptée de l'instrument. C'est en quelque sorte une forme de scénarisation du comportement potentiel de l'instrument à cette étape d'écriture. Chaque rectangle muni d'une étiquette présente dans les schémas devra à un certain moment exécuter la fonction que l'ensemble lui demande d'accomplir. Étant donné que le détail du fonctionnement interne de la boîte n'est souvent pas connu et/ou testé à cette étape, on tend à utiliser le terme boîte noire pour y référer.

Le terme boîte noire désigne une constituante qui produit un résultat qui échappe à notre déduction logique. L’opacité que présente une boîte noire dans notre capacité de comprendre son fonctionnement peut constituer un frein si cette dernière est non documentée ou maîtrisée \citep{latour_science_1987}. Je considère les schémas que je produis comme des guides me permettant de résoudre graduellement les boîtes opaques. Le développement calqué sur la schématisation donne lieu à une nature modulaire des boîtes au sein de l'ensemble. Parfois il arrive que j'implémente plusieurs solutions pour accomplir une même fonction, cette approche permet d'alterner assez aisément entre différentes boîtes.

Les schémas que je conçois sont généralement organisés de manière à avoir une entrée et une sortie. Ces relations d'entrées et de sorties sont aussi évoquées par des flèches au sein de la relation entre les boîtes. Les intentions et leurs rapports sont énoncés de manière croissante partant du plus générique, soit la vue d’ensemble, pour ensuite se préciser et donner des détails sur des sections spécifiques. 

En créant le schéma *Électrocinétique, schéma cybernétique \ref{fig:cybernetique}*, j'ai cherché à mettre l’accent sur la présence d’un rapport de rétroaction double qui opère dans cette pièce. La première rétroaction implique l’être humain et l’expérience qu’il en retire, la deuxième rétroaction est celle qui opère à l’intérieur du système. Le deuxième schéma *Fonctionnement de l’algorithme d’action \ref{fig:mouvement}* , se concentre sur la particularité algorithmique de la fonction d’incrémentation liée à l’effort et celle de décrémentation liée au passage du temps. Je soulève ici la présence de rétroaction animant les réponses des différents médiums expressifs (audio, vidéo) en lien avec la variation du mouvement donné. L’effet de cette relation se précise dans le schéma *Dimension sonore \ref{fig:sons}*  et *Dimension visuelle \ref{fig:visuel}* qui se concentrent sur les subtilités de l’implémentation de la relation entre l’interaction, le son et l’image.

Le résultat de cette étape est de concevoir des schémas qui serviront à concrétiser les rapports de fonctionnement des composantes de la pièce tout en menant à une documentation des comportements de la création.  Par la schématisation, je cherche à conserver une trace de l’organisation de l’intention tout en documentant la conception.  


## Genèse de l’objet technique

L'invention naît de la relation avec son milieu, la schématisation me sert en quelque sorte à tenter de capter ces relations. C'est un processus qui aide à comprendre graduellement chacun des éléments avant que ceux-ci se concrétisent et deviennent ainsi de plus en plus codépendants. Pour décrire ce processus, Simondon donne l'exemple du développement du moteur à combustion, plus spécifiquement les moteurs anciens où « chaque élément intervient à un certain moment dans le cycle puis est censé ne plus agir sur les autres éléments » \citeyearpar[p. 21]{simondon_du_1958}.

Le fait d’isoler d’abord les différentes composantes a permis de mieux se concentrer sur leur fonction spécifique en se concentrant sur un seul problème. Cela permet également de substituer différentes approches. Ainsi, en tentant de résoudre un problème spécifique, il est possible de changer les composantes au fur et à mesure qu’il se concrétise. Le processus de concrétisation opère aussi graduellement à mesure que le processus de réduction amène l’objet à se condenser et que chacune des pièces devient de plus en plus codépendante, c’est-à-dire que leur fonction dans le système est pensée en relation avec les autres éléments.

Partant du schéma, je développe des composantes issues des blocs schématiques. Ces composantes sont cependant imparfaites au moment de leur création initiale. Au fur et à mesure qu’elles sont développées, elles s’organisent autour de l’objectif commun qu’est celui de concrétiser l'invention. 

Cette étape fait référence au principe d’ontogenèse \citep{simondon_individu_1964-1}, c’est-à-dire qu'un milieu virtuel fertile à l'invention devient existant s'il est observé et de là peut s'opérer un dialogue d'adaptation-concrétisation entre les composantes de ce milieu. Le passage qui explique le mieux ce à quoi je fais référence est le suivant :

> La concrétisation est ici conditionnée par une invention qui suppose le problème résolu; [...] c'est en effet grâce aux conditions nouvelles créées par la concrétisation que cette concrétisation est possible; [...] L'adaptation-concrétisation est un processus qui conditionne la naissance d'un milieu au lieu d'être conditionné par un milieu déjà donné ; il est conditionné par un milieu qui n'existe que virtuellement avant l'invention ; il y a invention parce qu'il y a un saut qui s'effectue et se justifie par la relation qu'il institue à l'intérieur du milieu qu'il crée [...] L'objet technique est donc la condition de lui-même comme condition d'existence de ce milieu mixte, technique et géographique à la fois. \citep[p.54-57]{simondon_du_1958}

J'applique cette réflexion, soit celle de supposer que l'invention est *résolue* en la schématisant. Je teste ensuite l'invention dans son contexte. Cette supposition en lien avec un processus de création permet d’entamer le processus de concrétisation en observant les interactions entre les différents *milieux associés* propres à l'invention. En utilisant le terme milieu associé, je fais référence aux relations entre l’humain, l’invention, les composantes et l'interaction de ceux-ci. Par itération entre développement, invention, observation je cherche à favoriser le processus d'adaptation-concrétisation guidé par la réduction de l'empreinte écologique et technique. Ce processus de concrétisation est directement inspiré de la volonté de favoriser la cohérence interne de l'objet technique évoqué par Simondon dans le passage suivant:

> L’objet technique concret, c'est-à-dire évolué, se rapproche du mode d'existence des objets naturels, il tend vers la cohérence interne, vers la fermeture du système des causes et des effets qui s'exercent circulairement à l'intérieur de son enceinte, et de plus il incorpore une partie du monde naturel qui intervient comme condition de fonctionnement, et fait ainsi partie du système des causes et des effets. \citep[p.46]{simondon_du_1958}

Guidé par la recherche de cohérence interne, du rapprochement avec le fonctionnement naturel de l'objet, mon processus de recherche-création appliqué aux instruments interactifs implique une schématisation. Cette forme de scénarisation entre en dialogue avec la concrétisation de l'objet technique en supposant que l'invention est résolue. Cela permet de se concentrer sur l'interaction de l'invention avec son milieu associé. En composant graduellement les composantes schématisées et en déployant ces dernières dans le milieu, cela accroît ma sensibilité aux interactions. Suite à l'observation, je peaufine les schémas de manière à mettre de l'avant les interactions observées. Ce processus itératif permet l'évolution de composantes qui à la base sont interchangeables et qui plus tendent vers la cohérence interne tout en cherchant aussi à réduire les ressources écologiques et énergétiques que son fonctionnement requiert.

## Méthodologie de développement itératif en milieux associés   

Pour créer cette recherche-création, j’ai développé une méthodologie basée sur une approche heuristique combinée à une écoute des interactions des composantes en milieu associé. Les composantes schématiques planifiées sont mises à l’épreuve, replanifiées remises à l'épreuve, et ce de manière récursive. Par milieu associé, j’entends ici une définition large qui englobe autant l’environnement dans lequel l’objet est déployé, mais aussi le rapport d'interaction que l’objet a avec les interacteurs et inversement l'interacteur avec l'objet.

La méthodologie qui émerge du processus actuel cherche à mettre les différentes composantes exprimées dans la schématisation en action là où leur usage se confronte au réel. Pour ce faire, j’ai intégré différentes composantes schématiques d’*Électrocinétique* à plusieurs projets sur lesquelles j’ai œuvré dans les dernières années. Pendant plus de cinq ans, j’ai créé et participé à des projets de création audiovisuels autant personnels que professionnels en gardant à l’agenda de développer certaines microcomposantes d’*Électrocinétique* afin de pouvoir les tester dans leurs milieux associés.

notamment : 

* *B-cycle*\citeyearpar{arseneault_b-cycle_2012}; expérience immersive où un vélo stationnaire agit comme interface (détection de passage d'aimants)
* *Rouge Mékong*, \cite{collectif_lebovitz_rouge_2013}; développement d'une approche d'instruments interactifs autonomes distribués supportant une narration immersive interactive
* *Pédalier à image* \citeyearpar{arseneault_pedalier_2013}; espace de projection vidéo extérieure interactive multiposte où des pédaliers permettent l'activation de la façade vidéo
* *RUSH* \citeyearpar{arseneault_rush_2014}; Utilisation d'un contexte de diffusion corporatif pour exprimer un point de vue artistique critique et où le mouvement sur des pédaliers agissait sur le système de poésie audiovisuel génératif
* *Les Marcels* \citeyearpar{arseneault_les_2014}; interactions lumineuses (DMX) issues du mouvement de roues (aimant) déployées sur plateforme économe (Raspberry Pi).
* *Irradier*, \cite{trudel_irradier.irradiate_2015}; schématisation et déploiement de matériaux (logiciel et matériel) interrelié  afin de rendre visible l'invisible.
* *Arbol* \citeyearpar{arseneault_arbol_2015}; Interaction lumineuse (DEL addressable) issue du mouvement de roues
* *Situations sonores*, \cite{faubert_situations_2016}; Programmation sonore distribuée, optimisée pour petite économe (Raspberry Pi)
* *Veloce* \citeyearpar{arseneault_veloce_2016}; Performance audiovisuelle pour vélo renversé
* *Movement Art *, \cite{burton_movement_2016}; élaboration d'un système d'opération (basé sur *GNU/Linux*) répondant aux contraintes de déploiement d'une installation interactive muséale distribué multi écrans
* *Intersidéral*, \cite{trudel_intersideral_2017}; programmation(C++) optimisée pour plateforme économe (Raspberry Pi) permettant l'affichage lumineux et vidéo génératif.

Ces projets m’ont amené à segmenter différentes parties logicielles et électroniques tout en me permettant de les voir interagir avec leurs milieux. Cela m’a permis de développer graduellement des composantes autonomes tout en apprenant les technologies et techniques nécessaires aux fonctionnements de ces dernières. Diviser les responsabilités des différentes composantes en segments entraîne la création de logiciels plus simples. La communication entre ces composantes (représenté par les flèches au niveau de la schématisation) est une méthode facilitant la substitution d'implémentations au cours du développement. Chaque élément autonome peut ainsi être retravaillé, amélioré puis réintroduit dans un projet ultérieur en demeurant parfois compatible avec le projet précédent.

Lorsque je développe une nouvelle composante, je l'aborde dans l'environnement où le résultat me paraît le plus logique à obtenir. Cette étape de prototypage des composantes permet de créer des segments autonomes simples dans le paradigme programmation que je maîtrise le mieux au moment où j’entame le processus. Cela implique souvent que ce n’est pas d’emblée la manière la plus concrétisée pour résoudre le problème. Cependant, cela permet d’avoir un élément fonctionnel, d’analyser son mode d’opération et d’ainsi procéder graduellement à des phases de concrétisation. Ce processus entraîne parfois la réécriture complète d'une composante afin de l'adapter à une plateforme de déploiement moins énergivore. Ayant une bonne idée de son fonctionnement en l'ayant observé fonctionner, la réécriture de la composante est souvent efficace, car l'objectif de réduire la consommation de ressources demeure la quête qui alimente le développement.

Je souhaite que les composantes informatiques et matérielles puissent s'associer à de nouveaux contextes en lien avec les étapes précédentes de schématisation et de genèse en structures fonctionnelles. L'objectif est de créer des composantes assez spécifiques qui réagissent à un langage commun. Langage qui permet aussi d'articuler de manière sensible les interactions. Afin de pouvoir substituer l’implémentation des composantes du système, je compartimente les fonctions en programmes autonomes et j’utilise un protocole de communication normalisé entre les composantes.  

Étant donné que plusieurs des composantes de mon instrument interactif sont de nature logicielle, j’ai développé une méthode de travail qui me permet de développer un ensemble de logiciels-prototypes qui échangent de l’information via un protocole réseau normalisé, soit l’Open Sound Control (OSC) \citep{wright_open_2002}.

Dans le cadre de ma démarche, je me suis fortement intéressé à la vitesse de rotation d’une roue. J’ai traversé plusieurs itérations à la fois matérielles et logicielles, mais chaque fois, la communication est restée sensiblement similaire soit un déclenchement de passage émis par protocole OSC. L’utilisation de ce protocole permet d’échanger en temps réel entre ordinateur, microcontrôleur et logiciel. Une des forces de ce protocole est que l’on peut nommer la valeur qui circule et ainsi lui donner un sens lisible. Des barres obliques peuvent séparer les groupes de messages afin de structurer la réception. L'exemple le plus commun est celui emprunté par les adresses web soit le Uniform Resource Locator (URL). Chaque barre oblique présente dans l'URL permet d'accéder à un sous-dossier de l'organisation d'un site web. En utilisant l'OSC, le même concept peut être appliqué. Ainsi, dans Électrocinétique, certaines composantes produisent des messages très courts étant donné que leur fonction est simple alors que d'autres logiciels émettent et peuvent recevoir des messages plus longs(utilisant plusieurs barres obliques). Deux exemples de messages circulant dans *Electrocinétique* sont documentés dans le tableau suivant. Le premier est un message transmis à chaque activation d'un passage de roue alors que le second est lié à un paramètre d'expression qui peut être modifié par une interface tierce, un message émit depuis un terminal ou un fichier de sauvegarde.


\begin{table}[htb]
\centering 
\caption{Exemples de messages OSC}
\begin{tabular}{@{}p{3cm}p{5cm}p{5cm}@{}}
\toprule
Transmission & Message & Effet \\ \midrule
gpioOSC.py	->oscillographe & /gpio/1 1	& à chaque passage d'un rayon, incrémenter la valeur d'interactivité\\
oscillographe & /oscillographe/audio\_io/ sampler\_0/player/volume 1.0	& modifier le volume de l'échantillonneur à 100%\\ \bottomrule
\end{tabular}
\end{table}


Quand j'ai entamé la création d'*Électrocinétique*, j’utilisais un système de détection de passage d’aimants pour estimer la vitesse de la roue. Un aimant était installé sur des rayons de la roue et un magnétomètre sur le cadre. Chaque passage de l’aimant devant le magnétomètre fermait le circuit ce qui provoquait un changement d'état, une lecture. J'utilisais la plateforme Arduino \citeyearpar{arduino.cc_arduino_2003}, plateforme de prototypage électronique *Libre*. Le Arduino permet notamment de programmer un comportement lié à une différence de potentiel électrique et établir une communication vers une autre composante. J'utilisais le Arduino pour relayer le passage de l’aimant via OSC vers un autre logiciel afin d’être traité. 

Ce processus de segmentation m’a conduit à l’élaboration d’un milieu associé de développement assez flexible qui me permet de réaliser les différentes parties schématiques graduellement tout en permettant une réutilisation de certains segments. En priorisant un mode d’échange par protocole normalisé (OSC), cela permet à chacune des composantes de se stabiliser/déstabiliser son milieu associé tout en contribuant à la concrétisation de l’instrument interactif.


## Interfaces en action

Pour capter du mouvement, j’ai développé du matériel électronique et logiciel que j’ai déployé dans leur milieu associé où j’ai participé à leur usage. J’ai également observé d’autres personnes interagir avec mes instruments interactifs. 
Un des aspects fondamentaux d’*Électrocinétique* est la relation entre le mouvement induit à l’interface qui affecte l'expérience audiovisuelle. Par interface, j’entends ici les objets qui, lorsque manipulés, communiquent leurs mouvements à une programmation logicielle qui les interprète.

Je m’intéresse plus particulièrement ici à retracer comment le module d’acquisition du mouvement a changé depuis le début de cette recherche-création. À la base, je me suis inspiré de ce qui était disponible pour les vélos pour estimer leur vitesse soit le système de détection de passage magnétique qui permet aux cyclomètres de donner une vitesse en kilomètre-heure.

J’ai donc initialement détourné des pièces provenant d’un cyclomètre, plus précisément l’aimant qu’on fixe sur les rayons et la partie fixée au cadre qui laisse passer le courant lorsqu’un aimant passe devant. J’ai utilisé ce système de captation de différentes manières : dans un système de pédalier (*RUSH*, *pédalier à images*) pour déterminer à quelle vitesse l’interacteur pédalait ou directement accroché à la roue (*Les Marcels*, *B-Cycle*). Ainsi, j’ai réutilisé le même mode d’acquisition en substituant le logiciel créant la réponse à l’interaction. J’ai ainsi pu étudier comment en captant la même donnée à différents endroits du vélo, je pouvais influer sur l’information sortante.

En mettant cette interface en relation à son milieu associé, je me suis rendu compte qu’il y avait des améliorations à faire afin d’augmenter la résolution de l’information servant à estimer la vitesse de rotation de la roue. Par résolution, j’entends la fréquence d’émission de données par le capteur. Le mode de lecture relativement limité, soit une donnée à chaque passage d’aimant, constituait un frein à l’expression sensible et continue de cette manipulation. J’ai d’abord augmenté le nombre d’aimants pour augmenter la résolution de l’information. Plus le nombre d’aimants tendait vers le nombre de rayons, plus j’augmentais la résolution du mouvement perçu. Mais l’on peut imaginer la complexification de l’installation avec l’augmentation du nombre d’aimants, ce qui ne produisait pas nécessairement un processus de fonctionnement optimal. 

Afin de ne pas avoir un aimant sur chaque rayon, en 2015, j’ai décidé de changer de mode d’acquisition et de migrer vers une lecture de distance infrarouge au lieu d'un magnétomètre. Avec ce mode d’acquisition, le capteur jusqu’alors en deux parties (capteur sur le cadre et aimant sur le rayon) ne nécessite plus qu’une partie. Le nouveau capteur envoie un signal sous la forme d’une lumière infrarouge qui va réagir à chaque passage de rayon venant l’obstruer. J’augmentais ainsi la fréquence de l’information tout en réduisant la complexité du matériel.

En substituant le mode d’acquisition, j’ai pu réutiliser la même plateforme (Arduino), et les mêmes logiciels et ainsi adapter les réglages à la nouvelle précision d’interaction. En changeant ce mode d’acquisition, cela m’a également permis d’avoir une meilleure résolution du mouvement en minimisant le nombre d’interventions physique sur la roue. J’associe cette transformation à un perfectionnement mineur comme le décrit ce passage du *mode d’existence des objets techniques* (MÉOT, 1958). Bien qu'essentiel pour accroître la sensibilité de l'instrument, ce perfectionnement m'éloignait du changement majeur qui avait besoin d'être opéré, soit celui de travailler directement avec le courant électrique généré par l'effort.

> La voie des perfectionnements mineurs est celle des détours, utiles dans certains cas pour l'utilisation pratique, mais ne faisant guère évoluer l'objet technique. Dissimulant l'essence schématique véritable de chaque objet technique sous un amoncellement de palliatifs complexes, les perfectionnements mineurs entretiennent une fausse conscience du progrès continu des objets techniques, diminuant la valeur et le sentiment d'urgence des transformations essentielles. \citep[p. 39]{simondon_du_1958}
 
En perfectionnant le module de captation, bien que de manière mineure selon Simondon, ce dernier est devenu plus concret. Moins de pièces étaient nécessaires à son fonctionnement tout en augmentant la relation d'interaction entre l'interface et l'expérience audiovisuelle. Ce développement me permit de  concentrer mes efforts sur le développement logiciel.

## Concrétisation logicielle

Ce sentiment d’urgence d’une innovation nécessaire s’est transmis dans mon approche des composantes supportant les logiciels que je crée. En tenant à tendre vers la réduction de l’électricité nécessaire pour faire fonctionner *Électrocinétique*, j’ai été confronté à changer de paradigme de programmation ainsi que d’architecture de processeur. Jusqu’en 2013, j’avais toujours développé mes logiciels et mes installations pour le système d’exploitation Macintosh en utilisant le logiciel Max/MSP \citep{cycling_74_max/msp_1997}  pour faire la programmation. Cependant, la quantité d’électricité qu’un ordinateur personnel consomme et le nombre important d’opérations pour configurer l’ordinateur afin qu’il se comporte comme un instrument interactif semblaient être deux symptômes d'un manque de concrétisation.

À cette période est arrivée sur le marché une plateforme qui allait changer mon rapport à la création d’instruments interactifs. Nommé *Raspberry Pi*, ce micro-ordinateur capable de rouler *GNU/Linux*, tenant dans la paume de main et coûtant environ 50$ canadien tout en consommant environ 5 watts d’électricité avait tout pour permettre le développement et le déploiement de mes instruments interactifs.  

Puisque je désirais réduire l’empreinte de l’ensemble du système, il m’est apparu primordial de pouvoir libérer mon ordinateur personnel de la responsabilité de faire fonctionner l’œuvre. Au cours des dernières années, le développement du matériel informatique a amené l’ordinateur à se miniaturiser et à s’optimiser grâce au développement des téléphones intelligents. Le ressac des millions investis en recherche et développement dans ce secteur a fait son chemin dans le monde des hobbyistes avec l’introduction de plateformes de développement (notamment le Raspberry Pi) basées sur l'architecture processeur utilisé dans les cellulaires. Cette architecture porte le nom de *ARM* soit *Advanced RISC machine*, où *RISC* signifie *Reduced instruction set computer*. Cette architecture se distingue de celle dans les ordinateurs personnels conventionnels par la complexité du registre d'instruction présent dans le processeur soit CISC (complex instruction set computer comprenant x86 et x64). L'avantage d'utiliser l'architecture *ARM* versus une architecture conventionnelle est son efficacité énergétique ainsi que son coût inférieur pour un système complet.

Depuis 2012, la plateforme Raspberry Pi s’est développée et est devenue de plus en plus mature autant du point de vue matériel, qu’au niveau du support des logiciels. Le changement d’architecture logicielle est toutefois potentiellement associé à un changement de paradigme de création. En effet, initialement mes outils pour créer et programmer n’étaient pas compatibles avec l’architecture *ARM*. 

Ce fut un processus assez long de traduire ma capacité de créer de manière confortable dans un logiciel de prototypage logiciel audiovisuel que je maîtrisais comme Max/Msp vers un équivalent inexistant sur le système *GNU/Linux* en plus de changer d’architecture *ARM* processeur.

Créer des projets sur cette plateforme entama un processus d'adaptations-concrétisation. Étant donné qu’aucun de mes projets conçus en Max/Msp ne pouvait fonctionner sur le système *GNU/Linux*, j’ai appris à programmer avec Pure Data \citep{puckette_puredata_1997}. Inspiré par le principe de logique câblée populaire chez les amateurs de synthétiseurs analogiques, le paradigme de programmation graphique proposé dans pure data ainsi que Max/Msp permet à l’utilisateur-programmeur d'inventer et modifier le comportement d’un logiciel en branchant/débranchant les entrées et les sorties de différents modules (objets) constituant la programmation.

Le paradigme de programmation qu'utilise ces logiciels (Data Flow) est un langage qui est interprété ce qui signifie que faire un changement sur la programmation ne nécessite pas de recompilation du code manipulé par le programmeur vers du code machine. Cela permet d'observer directement les manipulations effectuées au code. Notons tout de même la potentielle perte de performance par rapport à du code compilé spécifiquement pour répondre au comportement attendu.

En 2013, deux versions de Pure Data étaient disponibles, la version Vanilla développée personnellement par Miller Puckette et la version *Extended* supportée par la communauté. J’ai choisi d’utiliser la version *Vanilla* par souci de compatibilité et par volonté d’apprentissage. Celle-ci propose un nombre très réduit de fonctionnalités comparativement à la version étendue. Construire à partir d’un nombre réduit de fonctionnalités incluses dans le tronc principal a pour bénéfice d’accroître la compatibilité interarchitecture et la pérennité de la programmation. Généralement, en réduisant le nombre de dépendances externes cela contribue à un meilleur support à long terme.  Avec du recul, cette décision a été adéquate puisqu’elle a fourni une bonne méthode pour apprendre ce logiciel tout en permettant au projet de fonctionner à travers le temps. 

Après plusieurs mois d’apprentissage sur ce logiciel et le système d’exploitation *GNU/Linux*, j’ai déployé mon premier projet qui faisait usage de Pure Data et du Raspberry Pi. *Les Marcels* \citeyearpar{arseneault_les_2014} est une installation sons et lumières qui, à chaque quart de révolution d’une roue de vélo, produit un son et de la lumière. En misant sur le son et la lumière, je suis demeuré dans zone d'efficience de ma connaissance de Pure Data. En 2015, j’ai reproduit un principe similaire, mais avec beaucoup plus de lumières et davantage de sorties audios dans le projet *Arbol* \citeyearpar{arseneault_arbol_2015}. Ce projet m'a éloigné d'un déploiement sous *GNU/Linux* et Raspberry Pi, car il nécessitait davantage de ressources et certaines composantes qui m'étaient inaccessibles sous *GNU/Linux* via les outils que je maîtrisais pendant la création de ce projet. 

Face à cet obstacle, j’ai décidé d’entreprendre l’apprentissage d’un autre environnement de programmation qui pourrait supporter plus aisément un contexte de synthèse vidéo, soit *openFrameworks* \citep{lieberman_openframeworks_2006}. Cet ensemble de *librairies* facilite la création d’applications audiovisuelles. Le langage qu'il utilise est le C++ \citep{stroustrup_c++_1985}, c'est un langage qui nécessite une compilation à chaque changement du comportement, il a l'avantage d'être très optimisé au point de vue de la performance. L'apprentissage du C++ et le recours à *openFrameworks*  m’a permis d'accroître le registre expressif de la programmation visuelle ainsi que les performances tout en demeurant sur le duo Raspberry Pi et *GNU/Linux*.

## Plateforme de développement Libre, *GNU/Linux*

J’ai appris à aimer *GNU/Linux* car ce système me permettait de sculpter le comportement des instruments interactifs que je crée tout en fonctionnant sur un large registre de matériel informatique allant du très peu coûteux à l’autre extrême.
Une des dimensions fondamentalement différentes de ce système d’exploitation est que Linux est un noyau auquel se greffent les logiciels choisis par l’utilisateur pour créer le système dont l’utilisateur a besoin. Ce principe fondamental au système permet une concrétisation de ce dernier. Cela s'illustre par la possibilité d'installer un système très minimal et de le configurer de manière à ce que le comportement de la machine complémente l'invention tout en restant le plus minimal possible. Ce processus se fait par ajout des composantes nécessaires au lieu de devoir les supprimer d'un système destiné à un usage générique. 

Sur les systèmes destinés à des usages généraux (par exemple Window, Mac ou même Ubuntu), si l'on cherche à ce que le système ait un comportement très spécifique et optimal, on doit le restreindre d'accomplir des actions qu'il cherche à faire. Dans la distribution du système *GNU/Linux* que j'ai employé pour supporter ce projet soit *Archlinux* \citep{griffin_arch_2002}, le paradigme est le contraire. L'installation de base est la plus minimale possible avec presque rien d'activé par défaut. C'est la responsabilité du technicien d'assembler,installer, configurer, sculpter le comportement du système afin qu'il réponde à son milieu associé.  Cette approche me semble concorder avec le principe de concrétisation. 

En me lançant dans l’utilisation et l’apprentissage de *GNU/Linux*, j’ai été mis en contact avec un rapport au développement qui m’a particulièrement interpellé soit le *Logiciel Libre* ou *Free Software*.
Le développement des logiciels *GNU/Linux* est majoritairement mené dans une idéologie de partage et de liberté. Cette philosophie de développement se nomme *Free and Open Source Software(FOSS)*, aussi appelé *Logiciel Libre* en français.  Le *GNU manifesto* \citep{stallman_gnu_1985} est un pilier fondamental de ce mouvement. *GNU* est une collection de logiciels conçus pour permettre aux utilisateurs d'ordinateurs d’utiliser leur ordinateur avec des *Logiciels Libres* au lieu de dépendre de logiciels propriétaires. Le principe du *Logiciel Libre* se base sur quatre libertés essentielles \citep{stallman_gnu_1985}, soit: 


\begin{table}[htb]
\centering 
\caption{ \textit{GNU}: Libertés logicielles essentielles}
\begin{tabular}{@{}p{3cm}p{5cm}@{}}
\toprule
Liberté & Description 			\\ 	\midrule
0  & exécution				\\
1  & accès au code source		\\
2  & modification 			\\
3  & redistribution 			\\ 	\bottomrule
\end{tabular}
\end{table}


Initialement, *GNU* a été créé comme réponse à la dépendance grandissante à des logiciels dont le code source n’était pas disponible. Stallman mentionne dans le premier chapitre de Free as in Freedom \citep{stallman_free_2010}  qu’un des points de départ de ce manifeste aurait été la volonté de réparer une imprimante laser donnée au département où il étudiait au Massachusetts Institute of Technology (M.I.T) à Cambridge. L’imprimante en question avait tendance à bloquer pendant l’impression. Stallman avait déjà réglé ce genre de problèmes auparavant sur d’autres modèles d’imprimantes en modifiant le code source permettant l’impression. Cependant, sur cette imprimante, aucun code source n’était disponible et la politique de propriété intellectuelle de la compagnie empêchait l’obtention et la modification du code source ce qui aurait pu permettre au matériel d’opérer. Le fait de restreindre les modifications au fonctionnement de la machine est un obstacle à la concrétisation de l'objet technique, car cette action extrait l'humain du processus. 

En réaction à ce principe de sources fermées, le développement de *GNU* puis de *GNU/Linux* est créé de manière ouverte en respectant les quatre libertés évoquée précédemment. Cela a pour effet de favoriser l'accès à la documentation de la configuration du système. Par processus de configuration, *GNU/Linux* tend vers la concrétisation, seules les composantes essentielles au fonctionnement de l’expérience peuvent être employées afin de travailler ensemble. L'optimisation qui résulte du processus de sélection a pour effet de contribuer à l’efficience énergétique et matérielle de l’instrument interactif.  L’utilisation de ce système n’est pas nécessairement le chemin le plus rapide, mais c’est celui que je considère comme le plus transparent et flexible dans son mode d’opération, répondant à l’idée suivante proposée par Simondon :

>« L'homme comme témoin des machines est responsable de leur relation; la machine individuelle représente l'homme, mais l'homme représente l'ensemble des machines, car il n'y a pas une machine de toutes les machines, alors qu'il peut y avoir une pensée visant toutes les machines. » \citep[p. 145]{simondon_du_1958}

Le système entier est réfléchi comme des pièces détachées qui peuvent s’assembler de manière précise pour créer un ensemble qui échangera de manière très spécifique avec son milieu associé. C’est ce type d’approche qui m’a amené à embrasser l’apprentissage de *GNU/Linux* pour créer *Électrocinétique*. Développer sous ce système et cette architecture m’a amené à réfléchir autrement à la création d’instruments interactifs et me permet de mettre en application le principe de concrétisation de l’objet technique \citep{simondon_du_1958}.

## Documentation, communication et construction via GIT

De nombreux créateurs qui programment, dont moi, font face au problème de la gestion du code de leurs créations. Le code, comme le texte, est une matière malléable qui est amenée à changer au cours de l’écriture. Si plusieurs personnes travaillent à la réécriture d’un texte commun, il est facile de mettre au point un système de notation pour conserver chaque version en renommant les fichiers avec comme suffixe la date ou l’étape franchie afin de préserver une traçabilité ou un historique des modifications. Dans le cas du code, ce type de méthodologie, soit changer le nom d’un fichier est fortement contreproductive car elle peut mener au dysfonctionnement du logiciel. Si l’on tente de créer une analogie entre la compilation et l’impression, nous pourrions dire qu’un logiciel est un roman dont la moindre coquille, erreur de ponctuation ou majuscule manquante rendrait illisible l’intégralité de l’ouvrage. 

À cette rigidité du code s’ajoute la composante humaine. En effet, les individus qui rédigent du code ne travaillent pas toujours avec les mêmes méthodes. Cela peut engendrer certains problèmes lorsque ces individus tentent de mettre leur code en commun. Ce problème habite toutes les collaborations et est même au cœur de toute la communauté et la pensée *Free & Open-Sources*.

Dans ce domaine, GIT \citep{torvalds_git_2005} est devenu un des logiciels de gestion de versions les plus utilisés dans la communauté *Free & Open-Sources*. GIT a été créé par Linus Torvald en réaction à la non-disponibilité d’un logiciel de gestion de version libre pour supporter le travail collaboratif autour du noyau de Linux. GIT permet, entre autre, d’archiver et d’annoter tout changement effectué à du code en préservant la trace temporelle et celle de l’auteur. 

J’ai décidé d’apprendre GIT afin de me familiariser avec le langage, la communauté et la méthodologie de travail et ainsi améliorer mes compétences en matière de collaboration. J’ai donc commencé à utiliser GIT dans le cadre de mes projets personnels et professionnels ainsi que pour diverses collaborations artistiques.  J’ai aussi utilisé le service d’hébergement et de gestion de logiciels en ligne Github puis Gitlab afin de rendre le code d’Électrocinétique disponible.

Fonctionner avec ces outils a impliqué la création régulière de rapports sur les problèmes auxquels je faisais face. Cette documentation écrite en ligne prend la forme d’une *issue* \citep{github_mastering_2014}. En résumé, une *issue* gagne à contenir les éléments suivant (liste traduite librement du guide *mastering issue* \citeyearpar{github_mastering_2014} ):

* Un titre concis qui explique le problème rencontré
* Une description claire incluant une méthode de reproduction du problème et la configuration matérielle sur laque le problème est rencontré
* Des repères visuels (couleur) et des identifiants  (tags) permettant de faciliter la répertorisation par filtrage 
* Des objectifs de résolution temporelle (Milestone) afin d'organiser temporellement l'organisation du travail
* Une personne assignée responsable de la résolution de problème 
* Tout commentaire/conversation pertinent lié au processus de résolution du problème

En utilisant les outils utilisés par des communautés de développeurs j’ai pu garder un historique des modifications de mon code. L'apprentissage de ce mode de communication permet aussi de participer au processus collectif de concrétisation des outils dans la communauté *Free & Open-Sources*.

Au cours de mon travail avec le segment audio d'*openFrameworks,* j'ai fait mention de l'observation de comportements inattendus à la communauté. Ces observations se sont avérées à être des bogues qui furent réglés par un des programmeurs principaux d'*openFrameworks*, Arturo Castro.  Au cours du développement de la fonction d'interpolation de la vitesse sonore présente dans *Oscillographe*,  j'avais comme objectif de pouvoir jouer avec la vitesse de lecture d’un échantillon audio stéréo, de l’entendre adéquatement transposé et d’extraire en temps réel les valeurs d’amplitude sur les canaux de gauche et de droite respectivement. Le lecteur audio de base dans openFrameworks ne me permettait pas d’avoir accès aux valeurs brutes de l’échantillon, j’ai donc essayé plusieurs alternatives. J’ai alors été confronté à plusieurs échecs notamment dans l’implémentation du module ofxSoundObjects (Macdonald, 2017b) où j’ai rapporté sous la forme d’une issue \citep{ofxsoundobjects__16_weird_2017} comment l’utilisation de la fonction de *setSpeed* brisait la lecture du son. Environ 6 mois ont été nécessaires pour régler ce problème. Entre temps, j’avais isolé une implémentation d’un lecteur audio \citep{castro_ofxbasicsoundplayer_2017} qui me permettait d’avoir accès à ce dont j’avais besoin. Cependant, j’ai fait face à un nouveau problème, l’interpolation audio ne fonctionnait que lorsqu’il y avait un unique canal audio. Dès que l’échantillon était stéréo (deux canaux) et que la vitesse variait, la lecture audio ne se comportait pas comme prévu. 

J’ai d’abord contourné cet obstacle en créant deux lecteurs audio mono synchronisés, soit un pour le canal de gauche et un autre pour le canal de droite. Cette approche avait le défaut de rendre plus complexe ma gestion des échantillons, car je devais manuellement séparer les canaux en deux fichiers différents. J’avais aussi remarqué que ce problème n’était pas exclusif à l’implémentation *ofxBasicSoundPlayer*, il avait aussi lieu dans *ofxSoundObjects*. J’ai donc documenté ce problème ainsi que les étapes pour le reproduire sous la forme d’une issue \citep{ofxsoundobjects_21_[soundplayerobject]_2017}.

La conversation qui s’en est suivie a mené à la découverte et au réglage d’un bogue dans *openFrameworks*. Le problème a été résolu dans le tronc de développement maître via une *Pull request* \citep{macdonald_merge_2017}. Une *Pull request* est une copie du tronc principal comportant les modifications pour régler l'irrégularité où les changements menant à la résolution sont réintroduits dans le tronc principal. Suite au réglage de ce bogue, j’ai pu mettre à jour mon environnement de développement et revenir à un seul lecteur audio afin d’annuler les détours initialement pris. Je n’avais pas les connaissances pour régler moi-même le bogue dans les deux cas où j’ai émis des commentaires via Github. En utilisant une méthode de dialogue appropriée et adéquatement documentée, j’ai réussi à mobiliser suffisamment d’individus de la communauté pour améliorer le code que j’utilise. 

Comprendre l’évolution des outils et être capable de participer au processus de développement avec d’autres permet un effort collectif qui mène à une meilleure pérennité ainsi qu’à une compréhension plus précise des outils permettant la création.
 
L’utilisation du système d’exploitation *GNU/Linux* sous l’architecture *ARM*, de GIT comme logiciel de versionnage et la rigueur de son utilisation ont permis une meilleure traçabilité des changements, un archivage des étapes du développement ainsi qu’une capacité à comprendre et dialoguer avec d’autres créateurs d’outils et de logiciels.

# Dégagement des buts et des objectifs du mémoire 

En résumé, *Électrocinétique* est une pièce pour instrument interactif qui s’inspire du concept de la boucle de rétroaction et où sa recherche-création est basé sur la concrétisation de l’objet technique. Ce processus tend à s'assurer que l'objet technique proposé connaîtra son développement le plus adéquat tout en ne mobilisant que les ressources qui sont strictement nécessaires à son fonctionnement. La «réduction» opérée au cours de ce processus s'est manifestée à la fois par le recours à une plateforme très économe au niveau électrique ainsi que par la conception d'un logiciel le plus efficace et le moins énergivore possible.

La volonté de combiner différents éléments m’a amené à mettre en pratique le principe de concrétisation de l’objet technique afin de structurer l’approche évolutive du processus d’invention d’instruments interactifs ainsi que la maîtrise des outils mobilisée. J’ai d’abord segmenté les différentes composantes par schématisation afin de pouvoir étudier les interactions de l'instrument. Afin de faire fonctionner les fonctionnalités segmentées, j’ai créé des projets autonomes qui présenté sur des cycles relativement courts m'ont permis d'observer et d’analyser les interactions des milieux associés. 

Les contraintes associées à la réduction de la consommation électrique m’ont amenée à mieux maîtriser les outils que j’utilise. Pour créer cette œuvre, j’ai mis sur pied un environnement de création audiovisuelle à partir d'outils disponibles librement, en utilisant des matériaux de base très flexibles soit le C++ et *GNU/Linux*. Le tout en me familiarisant aux principes de collaboration sur du code source ouvert ainsi qu’à l’idéologie du *Logiciel Libre*.  

Cette approche de concrétisation teinte aussi le choix des matériaux qui constituent la pièce. L’idée est que l'expérience d'*Électrocinétique* soit une représentation temps réel des données audio entendues et où seules la différence de phase et l’amplitude construisent l’environnement visuel. Si les éléments soulevés dans ce chapitre abordent l’œuvre d’un point de vue technique, l’inscrivant dans une histoire de la cybernétique, de la concrétisation de l'objet technique et dans une pratique actuelle de la programmation d'instruments interactifs, le point suivant à aborder est son ancrage esthétique et conceptuel.


\chapter{Cadrage esthétique et culturel}

# Comparaison avec un corpus d’œuvres

Je m'inspire du travail de nombreux artistes, et ce, autant sur les plans conceptuels, visuels, sonores qu’en termes d’expérience proposée. En gardant à l’esprit les concepts étudiés dans le chapitre précédent, cette section présentera et expliquera comment certaines œuvres ont influencé mon travail. Je choisis ici plutôt que d’avoir un rapport analytique à ces œuvres, de dresser une liste structurée selon le type d’inspiration associé à chacune des œuvres abordées. Cette constellation permet d’éclairer les intuitions qui ont façonné *Électrocinétique*.

## Influences conceptuelles : dispositifs critiques pour mouvements

En utilisant le mouvement pour interagir avec l'œuvre, je me suis inspiré notamment du travail de l’artiste français Yann Toma. Il réalisa l'installation *Dynamo-Fukushima* \citeyearpar{yann_toma_dynamo-fukushima_2011} dans laquelle l'effort de plusieurs cyclistes permettait d’illuminer l’espace d’exposition du Grand Palais à Paris. Cette œuvre propose une réflexion critique sur la fragilité de l’existence plus précisément dans le contexte du rapport de dépendance énergétique qui caractérise la vie contemporaine. En soutien aux victimes de Fukushima, l’artiste a proposé aux participants de prendre leurs responsabilités énergétiques en pédalant sur ces vélos. Le discours est critique vis-à-vis l'usage du nucléaire, mais souhaite tout de même mettre en place une atmosphère festive :

Je me suis également inspiré par l'installation *Champs de Pixels* \citeyearpar{nova-lux_champs_2010} d’Érick Villeneuve et de Jean Beaudoin. Présentée dans le Quartier des spectacles de Montréal, elle était notamment activée et alimentée par les efforts des usagers sur des Bixis stationnés. De cette pièce, que j’ai eu le plaisir d’expérimenter, je retiens l’usage novateur de vélos libre-service présentés comme interface active qui alimentait le résultat par l’effort collectif. 

*Agit P.O.V.* \citep{castongay_agit_2013} est un autre projet destiné à démocratiser certaines notions d’électronique qui a retenu mon attention. *Agit P.O.V.* est un dispositif-œuvre qui incite les gens à s’exprimer grâce à un module programmable lumineux installé sur leur roue de vélo. Le processus de diffusion de l’œuvre inclut le partage du savoir-faire par des ateliers où les participants assemblent et apprennent à programmer l’objet. La documentation, le code et les schémas électroniques de ce projet sont disponibles sur internet, ce qui rend sa reproductibilité plus aisée. Cette perspective de démocratisation des connaissances et d'expression artistique critique en utilisant de la technologie a motivé l'adoption du Libre comme vecteur de développement pour *Electrocinétique*.

Concernant l'utilisation d'une roue comme objet artistique manipulable, je me suis directement inspiré de Marcel Duchamps \citeyearpar{duchamps_roue_1951} issue de sa série *ready-made*. Dans la filiation de l'idée de présenter un objet utilitaire comme matériel artistique devenant une œuvre, je me suis intéressé à construire une dynamique interactive qui transforme le comportement attendu de l'objet en une expérience audiovisuelle à découvrir. 

Sur le point de vue de la circularité entre les différents médiums, je suis particulièrement fasciné par *Very Nervous System* \citep{rokeby_very_1986}. Dans cette œuvre, le mouvement est analysé par un logiciel couplé à une caméra vidéo pointée sur une zone où un performeur peut bouger. L’ensemble produit du son en fonction du mouvement. Dans cette œuvre il se développe une relation d’écoute entre le mouvement du corps et le son produit chez le performeur.  

## Influences sonores : rétroaction comme matière, roue comme instrument

C’est par le son que j'ai souhaité établir un lien corporel entre l’usage et l’usager. La réponse du dispositif doit être instantanée et expressive afin de révéler le potentiel de l’instrument sensible. Je cherche à développer le plus possible l'instrument musical afin d’obtenir une identification parfaite entre l’action et le résultat sonore.

La relation entre le *feedback* sonore et le mouvement trouve également des échos dans différentes œuvres. Je pense notamment à la pièce *Pendulum Music* \citep{reich_pendulum_1968} où un microphone est suspendu par son fil au-dessus d’un haut-parleur dans lequel il est branché. Un mouvement de pendule est induit au microphone afin de le faire passer momentanément à proximité du haut-parleur ce qui résulte en un court moment d’accumulation de feedback. Le rythme qui émerge du mouvement du micro et le travail de cette matière fragile qu’est le feedback m’inspirent.

Dans la dynamique du travail sur le *feedback*, je suis inspiré par les timbres et le processus qui est à la genèse des pièces *Microphones* \citep{tudor_microphone_1973}. Travaillant directement avec le feedback, l’auteur des pièces performe un dispositif de filtres et de mix pendant de longues sessions. Le son se sculpte peu à peu laissant émerger des moments complètement frénétiques entrecoupés de courts silences un peu comme des respirations. La spécificité de la réponse acoustique des enregistrements des pièces issus de ce processus est directement liée à la résonance acoustique des lieux où sont réalisées les séances de performances.

D’un point de vue rythmique, je souhaite établir une relation symbiotique avec l'interaction tout en restant relativement minimaliste et génératif. Inspiré par les propos recueillis dans *L’art du Trompe-l’Oreille Rythmique* \citep{feron_lart_2010}, je m'inspire du travail sur la structure rythmique de Terry Riley et Steve Reich afin de composer *Électrocinétique*.

## Influences visuelles : illumination et animation du mouvement 

La dimension visuelle de mon projet s’articule principalement autour de la visualisation du son produit par l’instrument. La représentation fidèle du phénomène sonore et la rapidité d’affichage seront privilégiées par rapport à la fidélité de reproduction du réel par exemple.

Sur cet aspect, je m’inspire notamment du travail de Norman McLaren et particulièrement de son film *Synchromie* \citeyearpar{mclaren_synchromy_1971}. Ce film explore la visualisation de la partie de la pellicule qui est destinée à être entendue soit la bande sonore optique. Les sons que l’on entend sont des interventions faites sur la pellicule, presque exclusivement des rectangles de différentes grosseurs à intervalles réguliers. L’aspect très épuré, le contraste élevé des éléments vus et la correspondance directe avec ce qui est entendu sont un exemple de synchronicité audiovisuelle particulièrement probant.

Au point de vue de la programmation visuelle, j'ai eu recours à plusieurs principes répertoriés dans *The Nature of Code* \citep{shiffman_nature_2012}, *Form+Code in Design, Art, and Architecture*  \citep{lust_form+code_2010} ainsi que *Learning Processing* \citep{shiffman_learning_2015}. Ces ouvrages, accompagnés de leurs exercices, m'ont permis d'explorer le registre expressif de la visualisation graphique issue d'algorithmes. Afin de pouvoir traduire les exemples traités dans ces livres vers le C++, j'ai approfondi ce langage avec *A Tour of C++* \citep{stroustrup_tour_2013}, *The C++ Programming Language* \citep{stroustrup_c++_2013}, *Programming : principles and practice using C++* \citep{stroustrup_programming_2014}. L'ensemble de ces documents a contribué à saisir le fonctionnement et certaines particularités propres au C++ notamment sa gestion très précise de la mémoire d'un logiciel. En parallèle de l'apprentissage de ce langage, je me suis familiarisé avec le fonctionnement d'*openFrameworks* en me basant sur ces manuels, *openFrameworks Essentials* \citep{perevalov_openframeworks_2015}, *Mastering openFrameworks : Creative Coding Demystified* \citep{perevalov_mastering_2013}, Programming Interactivity, \citep{noble_programming_2012} ainsi que *The Book of Shaders* \citep{gonzalez_vivo_book_2015}. L'ensemble de ces lectures ont contribué à transformer mon approche de résolution de problèmes impliquant la programmation logicielle. 

Dans la perspective de l’usage de la rétroaction pour générer un résultat visuel, j'ai été très inspiré par le travail de Jean Pierre Boyer et son instrument, le *Boyétiseur* \citeyearpar{boyer_lintegrale_1975}. Partant d’un phénomène observable, soit la distorsion provoquée par la proximité d’un champ magnétique adjacent au canon à électron dans un tube cathodique, l’artiste-chercheur a construit un instrument permettant d’intervenir sur ce phénomène. Autant par le résultat, que je trouve esthétiquement et conceptuellement intéressant, que par la démarche de recherche-création, le *Boyétiseur* est intimement lié aux processus d’*Électrocinétique*.

# Dégagement des aspects retenus pour l’œuvre

Pour créer la partie installation d’Électrocinétique, je m’inspire du travail à la fois critique et conceptuel d’artistes qui m’ont précédé et qui ont pris comme sujet l’objet du vélo ainsi que le mouvement afin de créer un objet qui s’active lorsqu’on lui transmet du mouvement.

Concernant le son produit, c’est le feedback audio qui demeure le générateur de son le plus près du concept de réinjection. Deux modes d’opération seront retenus pour l’instrument : la lecture d’échantillons où la vitesse est influencée par la manipulation de la roue ainsi que le mode d’analyse en temps réel d’une source directe.

À propos de la dimension visuelle, le travail de musique pour oscilloscope de Jerombeam Fenderson \citeyearpar{fenderson_oscilloscope_2016} a également été une inspiration importante. Son travail, à la fois visuel et sonore, consiste à produire des sons stéréophoniques qui sont analysés par un oscilloscope analogique. Sur son site se trouve une référence au logiciel intitulé *Oscilloscope* \cite{raber_oscilloscope_2018-1}. Ce logiciel programmé en *openFrameworks* permet de charger des échantillons sonores et de les faire jouer en faisant fluctuer la vitesse tout en affichant une représentation visuelle inspirée d’un oscilloscope analogique.

Alors que j’expérimentais avec ce logiciel, en y mettant mes sons, je me suis rendu compte de ses limites. Il dépendait notamment de plusieurs librairies externes qui rendaient son portage vers le Raspberry Pi très complexe. De là est née l’idée de la création d’un logiciel qui n’aurait pas ces limitations au niveau du déploiement. *Électrocinétique* est basé sur le principe de visualisation de la stéréophonie sur l’axe horizontal et vertical tout en tentant de limiter ses dépendances à des librairies incompatibles avec l'architecture *ARM*.


# Perspective communicationnelle et pertinence d’une approche de recherche-création

Mon instrument audiovisuel s’inscrit dans une vision particulière de la notion d'instrument. Il est joué au sens de découverte ludique d'un univers et au sens performatif du registre d'expression. J’inscris cette démarche dans la continuité d’artistes pratiquant la recherche et la création. J'ai compris ce processus lorsque cette recherche s’est développée à travers les méthodes d'expression esthétique que j'ai développées afin de favoriser l'émergence d'un registre. Cette curiosité envers la matière sonore difficilement contrôlable qu’est le *feedback* m’amène à apprivoiser cet élément et à créer des méthodes pour en extraire des expériences audiovisuelles.  

Concernant le matériel, travailler avec des micro-ordinateurs nécessitant beaucoup d’optimisations m’a forcé à mieux comprendre le comportement des fonctions élémentaires de ces machines. La faible capacité de calcul de ces machines offre un meilleur rendement énergétique, mais implique de penser leurs relations avec les éléments de l’ensemble de manière plus précise, de façon à sélectionner uniquement les éléments indispensables.

Ces considérations pratiques et conceptuelles m’amènent à réfléchir sur le processus de recherche-création d'instrument audiovisuel interactif, ses composantes, son processus d'assemblage, l'expérience ainsi que son registre d'interaction.


\chapter{Présentation et intentions de l’œuvre}

# Présentation de l’œuvre

## L'instrument

*Électrocinétique, réinjection cinétique en interactivité*, est une installation performée devant public. L’installation est composée d’un vélo à l’envers où chacune des roues influence des composantes de l’instrument-logiciel. L’instrument logiciel fonctionne sur un Raspberry Pi installé sur le vélo et relié à une carte de son. Cette carte son a la particularité d’être aussi un mélangeur audio que j’utilise comme un instrument afin de créer du *feedback*. Dans la mouture monocanale d’*Électrocinétique*, le *Raspberry Pi* est branché dans un projecteur vidéo afin de visualiser le rendu du logiciel (voir figures \ref{fig:o1_visuel}, \ref{fig:o1_plantation}, \ref{fig:o1_branchement}). Dans sa mouture immersive, la sortie audio du *Raspberry Pi* installé sur le vélo envoie ses canaux audio vers d’autres *Raspberry Pi* disséminés dans l’espace, chacun étant branché à un projecteur vidéo. La projection vidéo cherche alors à couvrir le maximum de surface pour créer un espace immersif (voir figures \ref{fig:o3_visuel_1}, \ref{fig:o3_visuel_2}, \ref{fig:o3_plantation}, \ref{fig:o3_branchement}).

## La performance

La performance est effectuée en deux temps. Le premier temps constitue la manipulation des roues de vélo. L’action sur la roue arrière a pour effet de faire fluctuer proportionnellement la vitesse de lecture d’un échantillon sonore. L’activation de la roue avant a pour effet une progression dans les différents chapitres de la composition en changeant notamment la zone de boucle de rétroaction qui est entendue tout en activant un lecteur secondaire et en changeants différents paramètres. Cela permet de progresser dans une composition tout en préservant une temporalité propre à chaque performance. 
Le second temps constitue une manipulation du mélangeur audio. Ce moment vise à jouer avec l'instrument, à explorer les fréquences, à trouver des sons, à improviser du feedback audio. La pièce se conclut en revenant sur une fréquence harmonique en forme de cercle. \footnotemark

Suite à la performance, la partie installation reprend vie. Une fois que les spectateurs ont vu la manipulation de l’instrument, ils peuvent, à leur tour, interagir avec l'instrument. Cela provoque un moment privilégié pour discuter, prendre les commentaires, partager et faire découvrir. 

\footnotetext{
la documentation de ces performances est disponible à l'URL suivant : \href{http://gllm.ca/projets/electrocinetique/}{gllm.ca/projets/electrocinetique}
}

## Diffusion, public visé, mise en valeur

La présentation de cette pièce est destinée à un public cherchant une expérience assez brute et expérimentale. Deux présentations ont déjà eu lieu soit au Cœur des sciences de l’UQAM dans sa mouture monocanal lors d'un concert à la suite d'une performance intitulé *Créatures hermétiques dans mon char bb des otages inaperçus* \citep{st-onge_creatures_2017}. La deuxième diffusion publique d'*Électrocinétique* a eu lieu au studio Artificiel à Montréal lors de la 33e édition des Vendredis Boullis \citeyearpar{artificiel_vendredi_2018}. À plus long terme, je cherche à diffuser dans des festivals tels que le Mois Multi (QC), Mutek (Montréal) et potentiellement présenter ce travail à l’international, tel qu'à Ars Electronica (Autriche) ou New Interfaces for Musical Expression (Brésil) par exemple. Au-delà de la performance, j’aimerais mettre en place des séances ateliers où je pourrais transmettre des notions que j’ai acquises au cours de ce processus. En effet, les concepts liés aux instruments interactifs, le déploiement et leur usage sur des Raspberry Pi, l’utilisation de *GNU/Linux* dans le cadre d’installations interactives, le traitement et l’analyse de sons sous *openFrameworks* ainsi que l’acquisition de données en utilisant des capteurs sont autant de connaissances à partager qui pourraient être utiles à d’autres créateurs.

## Démarche itérative

Lors de ma présentation de projet de mémoire, je m’étais fortement interrogé sur la tournure de mon projet final. En effet, j’ai dû momentanément mettre de côté l’autonomie énergétique. L’instabilité de l’ordinateur était en effet trop grande avec comme seule source d’énergie le vélo, j’ai donc utilisé une batterie qui faisait l’intermédiaire entre le vélo et l’ordinateur, cette dernière était entièrement chargée avant le début de la présentation. Bien que cet objectif soit resté présent dans la perspective d’une innovation majeure dans le dispositif, je me suis concentré dans le cadre d’*Électrocinétique* à apprendre et à maîtriser les outils me permettant de créer et de déployer un logiciel-instrument sur un micro-ordinateur consommant peu d’électricité. Aussi, depuis le projet de mémoire, la forme de l’œuvre est devenue beaucoup plus claire. Lors de ma présentation de projet de mémoire, je n’avais pas encore déterminé si la forme finale de ce processus allait être une installation ou une performance. Finalement, ce sont les deux, une installation performée qui demeure accessible au public pour l'explorer. La dimension esthétique s’est aussi beaucoup précisée par rapport au projet de mémoire. Je n’étais, lors de cette première présentation, pas fixé sur la méthode de représentation de l’interaction, je voulais qu’elle soit sentie le plus directement possible. En partant du son et en le rendant visible en utilisant de la lumière vidéo, je sens que j’ai fait progresser mon idée d'origine.

# Compte rendu des présentations et retour critique 

## Retour sur la réception de l’œuvre

Suite à la première diffusion au Cœur des Sciences d'*Électrocinétique*, j'ai eu un bon retour sur des aspects clés de mon installation-performance. Du point de vue de la performance, la relation entre le vélo et le mélangeur n'était pas claire pour certains membres du public. Il faut dire que je me sentais un peu dispersé entre le mélangeur audio générant du feedback, un contrôleur midi qui affectait les paramètres du logiciel ainsi que les roues de vélo qui influençaient le volume et la vitesse d’un échantillon. 

J’avais recours à beaucoup de matériel pour contrôler beaucoup de paramètres du logiciel servant à générer le visuel *Oscillographe*. Avec du recul, cette quête du contrôle était finalement à contre sens d'une volonté de concrétisation de l'objet technique. Ce désir de contrôler tous les paramètres de l'instrument constituait une distraction qui affectait ma qualité de présence comme performeur. J’ai donc cherché à améliorer le rapport à l'instrument lors de la deuxième présentation. Pour ce faire, j'ai soustrait certains éléments de contrôle et attribué la plupart des changements de paramètres à la roue avant du vélo. Ainsi, chaque roue avait une relation un peu plus claire; la roue arrière affectait la vitesse de lecture de l’échantillon audio, alors que la roue avant permettait de changer les paramètres du logiciel.

Lors de la deuxième présentation, j'ai aussi augmenté le nombre de sorties vidéos à trois ce qui rendait l’ensemble plus immersif. J’ai aussi augmenté la vitesse de rafraîchissement vidéo en désactivant le système de fenêtrage sur la Raspberry Pi. La composition spontanée et les images qu’elle a générées ont été bien reçues par le public qui est parvenu à s'immerger dans cette œuvre expérimentale quelque peu chaotique. Cette présentation a également soulevé la question de la matière avec laquelle je travaillais.

En effet, si des réflexions sur le *feedback* et la matérialité sonore des machines étaient à la base du projet, elles ne transparaissaient pas nécessairement dans le rendu final. Au cours des prochaines présentations de ce projet, je songe notamment à poursuivre le processus de clarification du rapport entre l’interaction et les sons produits. Je pense tout de même avoir rassemblé le matériel et acquis le savoir-faire pour l'opérer. Dorénavant je pourrai mieux me concentrer à créer et à composer à partir de ces outils et de cet environnement.


## Analyse rétroactive de l’ensemble de la démarche 

La démarche qui soutient *Électrocinétique* est axée sur la concrétisation de l’objet technique qui, dans ma vision, constitue une volonté de perfectionner l’intégration des composantes d’un dispositif permettant la création. L’installation-performance ainsi créée se construit par les rapports des différents éléments qui y participent. Leur codépendance est priorisée afin de constituer un assemblage technique qui bien que modulaire à la base, devient indivisible dans son fonctionnement. Chaque pièce sert à l’ensemble en diminuant au maximum le superflu. Ce processus de concrétisation tend aussi vers réduction matérielle et l’optimisation de l’usage des ressources autant matérielle qu'énergétique.
 
Revenons à l'exemple de la turbine Guimbale proposé par Simondon afin d’illustrer un objet technique concrétisé. Dans le cas d’*Électrocinétique*, le vélo, les capteurs, l’ordinateur, le système d’exploitation et le logiciel Oscillographe deviennent ici autant d’éléments qui agencés deviennent un objet technique qui tend vers la concrétisation.

La concrétisation est présente dans mon travail, et ce dans le rapport de complicité entre l’interface physique, les capteurs électroniques, le matériel informatique, le système d’exploitation ainsi que le logiciel qui est exécuté. En utilisant une plateforme dotée d’une architecture processeur très peu énergivore, en assemblant un système d’exploitation qui accomplit exclusivement les tâches essentielles au système et à l’instrument, en concevant un logiciel et un programme informatique en utilisant du code machine optimisé pour cet ensemble, *Électrocinétique* tend vers la concrétisation de l’instrument interactif.
\conclusion{}

*Électrocinétique* a été le prétexte d'un développement de la pratique combiné à la théorie par l'entremise d'une recherche-création. Si nous présentions le projet rapidement en introduction, ce que notre développement démontre est la nécessité d'un processus de réflexion combiné à des créations afin de tendre vers la concrétisation. L'idéal du performeur chevauchant son vélo et pouvant déployer un environnement audiovisuel immersif où l'effort produit l'électricité et active l'expérience interactive demeure un objectif comportant encore plusieurs étapes à franchir. La préoccupation de départ était de supprimer tous les éléments superflus afin de concrétiser le logiciel et le matériel. C'est par l'expérimentation et l'erreur que j'ai pu perfectionner le dispositif en passant par exemple d'un capteur magnétique à un capteur infrarouge. Je n'aurais pas pensé à cette nouvelle solution sans l'étape préalable qui m'a fait comprendre l'inutilité de la multiplication du nombre d'aimants au sein d'une dynamique de simplification. De la même façon, jusque-là habituée à des architectures numériques complexes, je me suis orienté vers Linux sur l'architecture *ARM*, me dépouillant ainsi d'outils de création avec lesquels je créais. 

La pensée de Simondon a stimulé l'apprentissage du C++, Pure Data, GIT et *openFrameworks* afin de consolider un environnement de création audiovisuel Libre et optimisé. L'ensemble facilite le déploiement de projets sur Linux en utilisant au mieux des ressources limitées tout en adhérant davantage à la «culture du libre». C'est au sein de ce nouvel environnement que j'ai pu construire un milieu qui favorise la réponse de chaque élément aux autres et à l'ensemble. Mais *Électrocinétique* reste, avant tout, une œuvre inachevée, mobile et en perpétuelle concrétisation.

Chacune des itérations de cette pièce constitue une mise à l'épreuve qui tend vers un perfectionnement continu. À chacune des étapes du développement, elle s'est enrichie par ses réussites et par ses échecs. Toujours dans la perspective d'une concrétisation de l'objet technique, je ne cesse de développer des savoirs, de trouver de nouvelles méthodologies, d'en abandonner certaines pour en suivre de nouvelles qui se sont parfois avérées si pertinentes qu'elles m'ont fait prendre un virage significatif donnant une dimension nouvelle à l'œuvre. 

Toutefois ces recherches n'ont pas toujours été couronnées de succès, le processus a souvent pris des directions qui m'ont forcé à suivre d'autres pistes. Cependant, si nous pouvions considérer qu'il s'agissait là d'égarements, force est de constater l'apport important qu'ont eu ces moments dans le recadrage du projet et la mise en évidence de la nécessité de trouver d'autres solutions.

Dans cette idée, *Électrocinétique* me semble avoir été et être encore un projet auquel l'idée de recherche-création s'applique parfaitement. Travaillant la réinjection, élément mis en valeur assez tôt dans l'histoire des nouvelles technologies de communication, le projet montre à quel point cet élément demeure fertile dans la perspective d'une production artistique. 

L'installation audiovisuelle et les performances que j'ai mises en place sont autant de possibilités d'exploration de la matérialité d'un médium dont la profondeur est parfois négligée. J'ai dû apprendre à avoir un rapport particulier avec la machine, approfondissant ma connaissance de celle-ci pour répondre à une volonté de concrétisation dont une clé est la meilleure compréhension de l'interaction entre chacun de ses maillons. 

Bien que le projet ait connu des progrès considérables depuis sa genèse, il serait difficile de considérer *Électrocinétique* comme un produit fini. En effet, qu'il s'agisse de la composition de mes performances qui n'en est qu'à ses prémisses, tant mon utilisation de l'instrument qui commence tout juste à être maîtrisé ou de l'autonomie électrique qui n'est pas encore atteinte, un effort de concrétisation peut et doit encore être fourni.

Je pense qu'aborder le processus de concrétisation de l'instrument interactif dans le cadre d'une recherche-création constitue le fondement d'une démarche qui dépassera le projet d'*Électrocinétique*. Créer en cherchant à employer moins de matériel technique tout en comprenant mieux son fonctionnement, ses comportements, ses forces et ses limites sont des vecteurs qui définiront mes créations futures. Dans cette perspective de développement d'instrument interactif, le concept de *Lutherie Numérique*, \cite{burton_lutherie_2000} demeure un sujet foisonnant qui gagnerait à être davantage approfondi et documenté. 

Concernant le sujet de la cybernétique, il serait intéressant d'analyser le processus de création d'instruments interactif sous la lunette de Gordon Pask. La lecture qu'en fait Margit Rosen dans *Gordon Pask's Cybernetic Systems: Conversations after the End of the Mechanical Age* \citeyearpar{margit_gordon_2016} est particulièrement applicable à ce sujet.

D'autre part, la collaboration avec des communautés de développeurs m'a permis de réaliser à quel point le partage de connaissances et l'utilisation de logiciels Libres pouvaient être à la fois constructifs et politiques dans un contexte de création. Le sujet des enjeux éthiques soulevés par le développement communautaire de *Logiciels Libres* est traité en profondeur dans *Coding freedom: the ethics and aesthetics of hacking* \citep{coleman_coding_2013}.

Au-delà d'un intérêt personnel à créer davantage d'instruments-logiciels Libre basés sur le *Raspberry Pi*, l'envie de transmettre ces techniques s'est également développée pendant le projet. J'aimerais, à l'avenir, réinvestir le savoir accumulé dans mes travaux, mais également m'impliquer davantage dans la communauté en pensant à l'apport que la diffusion et l'échange peuvent avoir sur la concrétisation de l'objet technique. Concernant le recours à la pensée de Simondon, le présent document ne fait qu'effleurer la surface d'une réflexion complexe et complète. Il serait très envisageable d'approfondir les concepts d'*individuation* \citep{simondon_individuation_1989-1} et de *transduction* \citep{simondon_individu_1964-1} dans un processus d'invention d'instrument interactif.

*Électrocinétique* évoluera encore dans les années à venir. L'œuvre apportera également, je l'espère, certaines innovations qui pourront être développées par d'autres artistes dans le contexte d'autres créations, poursuivant ainsi la démarche entamée par les chercheurs-créateurs mentionnés dans ce mémoire et que j'ai voulu poursuivre. 
\renewcommand{\appendixname}{Annexe}
\appendix
\chapter{Projets associés}

\begin{figure}
\centering
\caption{\textit{B-Cycle}, 2012}
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/00_projets/img_bcycle_01.jpg}
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/00_projets/img_bcycle_03.jpg}
\label{fig:bcycle}
\end{figure}

\begin{figure}
\centering
\caption{\textit{Pédalier à images}, 2013}
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/00_projets/img_pedalier_01.jpg}
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/00_projets/img_pedalier_03.jpg}
\label{fig:pedalierimages}
\end{figure}

\begin{figure}
\centering
\caption{\textit{Les Marcels}, 2014}
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/00_projets/img_marcels_01.jpg}
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/00_projets/img_marcels_02.jpg}
\label{fig:marcels}
\end{figure}

\begin{figure}
\centering
\caption{\textit{RUSH}, 2014}
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/00_projets/img_rush_01.png}
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/00_projets/img_rush_02.png}
\label{fig:rush}
\end{figure}

\begin{figure}
\centering
\caption{\textit{Arbol}, 2015}
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/00_projets/img_arbol_06.jpg}
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/00_projets/img_arbol_10.jpg}
\label{fig:arbol}
\end{figure}

\begin{figure}
\centering
\caption{\textit{Veloce}, 2016}
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/00_projets/img_veloce_07.jpg}
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/00_projets/img_veloce_01.jpg}
\label{fig:veloce}
\end{figure}

\chapter{Turbine de Guimbal}

\begin{figure}
\centering
\caption{Générateur et turbine combinée, \cite{guimbal_combined_1953}}
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/00_turbine/US2634375_1.png}
\label{fig:guimbal_1}
\end{figure}

\chapter{Schématisation \textit{Électrocinétique}}

\begin{figure}
\centering
\caption{Schématisation cybernétique}
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/01_schema/cybernetique}
\label{fig:cybernetique}
\end{figure}

\begin{figure}
\centering
\caption{Fonctionnement de l'algorithme d'action}
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/01_schema/mouvement}
\label{fig:mouvement}
\end{figure}

\begin{figure}
\centering
\caption{Dimension Sonore}
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/01_schema/sons}
\label{fig:sons}
\end{figure}

\begin{figure}
\centering
\caption{Dimension visuelle}
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/01_schema/visuel}
\label{fig:visuel}
\end{figure}

\chapter{Déploiement Monocanal}

\begin{figure}
\centering
\caption{Monocanal, visuel}
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/02_oscillo/o1_visuel_1.jpg}
\label{fig:o1_visuel}
\end{figure}

\begin{figure}
\centering
\caption{Monocanal, plantation}
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/02_oscillo/o1_plantation}
\label{fig:o1_plantation}
\end{figure}

\begin{figure}
\centering
\caption{Monocanal, branchements}
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/02_oscillo/o1_branchements}
\label{fig:o1_branchement}
\end{figure}

\chapter{Déploiement 3 Canaux}

\begin{figure}
\centering
\caption{Multicanaux, visuel}
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/02_oscillo/o3_visuel_1.jpg}
\label{fig:o3_visuel_1}
\end{figure}

\begin{figure}
\centering
\caption{Multicanaux, visuel}
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/02_oscillo/o3_visuel_2.jpg}
\label{fig:o3_visuel_2}
\end{figure}


\begin{figure}
\centering
\caption{Multicanaux, plantation}
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/02_oscillo/o3_plantation}
\label{fig:o3_plantation}
\end{figure}

\begin{figure}
\centering
\caption{Multicanaux, branchements}
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/02_oscillo/o3_branchements}
\label{fig:o3_branchement}
\end{figure}
\renewcommand{\appendixname}{appendice}
\appendice


\chapter{Ars Electronica 2018}

Suite au dépôt du mémoire, *Électrocinétique* fut présenté à Ars Electronica 2018. *Feedback Cycles for Oscillographes* fut choisi comme nom d'œuvre à l'international.

\begin{figure}
\centering
\caption{Extrait du site web \citep{hexagram_campus_hexagram_2018}}
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/03_ars/hexa/ae_campus_taking_care}
\label{fig:arsHexa}
\end{figure}


\begin{figure}
\centering
\caption{Extrait du catalogue \citep[p.227]{ars_electronica_ars_2018} }
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/03_ars/catalogue/extrait_catalogue}
\label{fig:arsCatalogue}
\end{figure}


\begin{figure}
\centering
\caption{Extrait du programme court \citep[p.44]{ars_electronica_timetable_2018} }
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/03_ars/catalogue/extrait_programme_court}
\label{fig:arsProgram}
\end{figure}


\begin{figure}
\centering
\caption{Performance Music Monday \citeyearpar{ars_electronica_music_2018} }
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/03_ars/selection/ars_1}
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/03_ars/selection/ars_2}
\label{fig:ars1}
\end{figure}

\begin{figure}
\centering
\caption{Performance Music Monday suite \citeyearpar{ars_electronica_music_2018}}
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/03_ars/selection/ars_3}
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/03_ars/selection/ars_4}
\label{fig:ars2}
\end{figure}


\begin{figure}
\centering
\caption{Rencontre avec Roy Macdonald}
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/03_ars/selection/rencontre_avec_Roy}
\label{fig:arsRoy}
\end{figure}



\begin{figure}
\centering
\caption{Branchements}
\includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{src/img/03_ars/schema/oscillosaltzam}
\label{fig:arsBranchements}
\end{figure}

\chapter{Documents d'appel}

Suite à la présentation d'*Électrocinétique* à *Ars Electronica*, une étape de rédaction/condensation permit de cadrer l'oeuvre dans des cases de formulaires d'appel de dossier. 

\newpage

**Description** 

*Électrocinétique* est une installe-action où un vélo est transformé en instrument sonore. Le mouvement exécuté sur son pédalier produit l'énergie nécessaire afin d'alimenter un mélangeur audio qui réinjecte ses signaux de sortie dans ses entrées. Le registre expressif de cet instrument volontairement instable est proportionnel à l'énergie résultant de l'effort fourni. 
Sur les murs de la pièce est projetée l'analyse du signal audio produit depuis une multitude de projecteurs vidéo. Les particularités sonores sont alors visuellement révélées. En résulte une synesthésie audiovisuelle hypnotique de lignes qui se tracent en fonction de la disparité stéréophonique du son que l'on entend. 
Le balai de formes et de sons est activé quotidiennement par le performeur et laissé disponible le reste du temps aux curieuses et curieux qui voudrait révéler l’intangible situé entre la discontinuité et le court-circuit.


\newpage

**Devis technique **

**Audio**

* 4 Haut parleurs actifs 
	* QSC k8.2 ou relativement équivalent en termes de réponse/présence
	* seront placé au sol tel que des moniteurs 
	* si haut-parleurs passif, autant de canaux d'amplification
* 1 Subwooffer actif  
* Un mélangeur audio disposant d'au moins 4 entrées et de 4 sorties (moniteur+main mix) 
* Filage approprié pour couvrir les distances entre le mélangeur en régie et les haut-parleurs.  


**Vidéo**

* 2 projecteurs vidéo récents HD
* ~5 projecteurs vidéo moins récent  
	* la diversité des projecteurs, leur âge et leur défaut sont des qualités intrinsèques au projet.
	* doivent être dotés d'une entrée vidéo analogue (composite)
* Câble d'alimentation pour chacun des projecteurs

**Électricité**

* 2 circuits électriques 15 Ampère 120v 
	* 1 dédié à l'audio
	* ~1 pour la vidéo (relatif au wattage total des projecteurs)
* Lot de filage/extension AC (U-ground) et multiprises

**Scénographique**

* 1 vélo de grandeur standard, mécanique fonctionnelle 
* 1 structure capable de tenir le vélo lorsqu'il est à l'envers sur ce dernier 
	* l'objectif ici est que la pédale soit à hauteur accessible 



\chapter{Outils de publication du mémoire}

Le présent document utilise l'outil de composition \hologo{LaTeX} \citep{latex3_team_latex_1983} et la classe memoireuqam1.3 \citep{legault_classe_2018}

La source du présent document \citep{arseneault_electrocinetique_2018} est hébergée à l'URL suivant :

* \url{https://gitlab.com/gllmar/electrocinetique_txt/}

La documentation audiovisuelle de l'œuvre est rassemblée à l'URL suivant : 

* \url{http://gllm.ca/projets/electrocinetique/}
