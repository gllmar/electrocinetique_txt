\introduction{}

\hypertarget{origine-intuxe9ruxeat-et-intentions}{%
\section{Origine, intérêt et
intentions}\label{origine-intuxe9ruxeat-et-intentions}}

Depuis plusieurs années, je me passionne pour la création
d'installations audiovisuelles produisant des expériences à partir de
l'interaction entre le mouvement corporel sur un mécanisme doté de
senseurs électroniques et de la programmation logicielle. \emph{B-cycle}
\citeyearpar{arseneault_b-cycle_2012}, \emph{Pédalier à image}
\citeyearpar{arseneault_pedalier_2013}, \emph{RUSH}
\citeyearpar{arseneault_rush_2014}, \emph{Les Marcels}
\citeyearpar{arseneault_les_2014}, \emph{Arbol}
\citeyearpar{arseneault_arbol_2015}, \emph{Veloce}
\citeyearpar{arseneault_veloce_2016} sont autant de projets qui
impliquent le mouvement d'un corps en relation avec la mécanique d'un
vélo , produisant ainsi une expérience audiovisuelle, parfois immersive.
Tous ces projets m'ont amené à réfléchir aux technologies que j'emploie
pour créer ces moments d'expérimentation et ces espaces d'interactions.

Ces assemblages complexes de technologies, de logiciels et de matériaux
tendent à s'inspirer du principe d'instrument de musique qui interagit
avec ceux qui le jouent. Je choisis de nommer cet ensemble «~ instrument
interactif ». Les instruments interactifs interagissent avec les
mouvements de l'usager par la production de lumière, de sons ou de
projections vidéo.

J'utilise des composantes de vélo comme interfaces accessibles aux
interacteurs\footnotemark, afin de les inciter à fournir des mouvements
familiers comme ceux de pédaler ou de faire tourner une roue. Au gré des
projets, j'ai développé du matériel électronique qui sert à capter le
mouvement lors de l'activation du dispositif. Ces composantes
électroniques communiquent avec des logiciels que je programme, qui
servent à transformer l'action et ainsi révéler le «~comportement » de
l'instrument. Le matériel électronique transforme un mouvement ressenti
par l'usager en flux d'information numérique. Ce flux de données
alimente une programmation logicielle qui se traduit en sons audibles,
en réactions lumineuses et en images vidéo projetées.

\footnotetext
{
J’utilise le terme « interacteur » pour décrire à la fois l’opérateur technicien \citep{simondon_du_1958} qui utilise la machine avec une connaissance de son comportement ainsi que l’usager qui le découvre. La différence entre ces deux types d'interacteurs est la compréhension préalable du fonctionnement de la boîte de noire \citep{latour_science_1987}. Ce qui les relie est leurs manipulations de l'interface qui permet à la machine de révéler son contenu \citep{paquin_comprendre_2006}.
}

Avec du recul, chacun de ces projets constitue une étape importante dans
la concrétisation d'\emph{Électrocinétique}. Mon intérêt à comprendre le
matériel que j'utilise m'a amené à utiliser moins de matériel, mais à
mieux le maîtriser. Alors qu'avec \emph{B-Cycle} (2012), j'avais besoin
de deux ordinateurs récents, trois projecteurs vidéo, une équipe de cinq
personnes, un outillage lourd ainsi qu'une programmation complexe et
fragile, en 2016, je présentais \emph{Véloce} avec beaucoup moins de
matériel (un ordinateur portable et un projecteur), et ce en étant
auteur de l'entièreté du code. Outre la technologie informatique qui
devient de plus en plus performante et miniaturisée, c'est surtout ma
maîtrise progressive des matériaux et de la programmation qui m'a amené
à être plus agile, conscient et autonome dans mes déploiements.
\emph{Électrocinétique} s'inscrit en continuité de cette démarche; mieux
comprendre pour réduire l'empreinte et ainsi optimiser le processus afin
de révéler un système plus efficace.

\hypertarget{formulation-du-probluxe8me-et-ancrage-thuxe9orique.}{%
\section{Formulation du problème et ancrage
théorique.}\label{formulation-du-probluxe8me-et-ancrage-thuxe9orique.}}

L'objet de ma recherche-création intitulé \emph{Électrocinétique :
réinjection cinétique interactive} consiste à créer une pièce
audiovisuelle à partir d'un dispositif matériel et d'une programmation
logicielle qui tendent vers l'autonomie électrique tout en demeurant les
plus expressifs possible, à la manière d'un instrument de musique.

En créant \emph{Électrocinétique}, je cherche à faire cheminer ma
réflexion critique autour de la technique. Entre alors en jeu l'idée
d'empreinte technologique qui correspond à l'ensemble des ressources
matérielles, humaines et électriques nécessaires au fonctionnement de
l'installation. Ma méthodologie de recherche et de création ainsi que ma
maîtrise des outils permettent de créer des instruments interactifs qui
tendent, avant tout, vers la réduction de cette empreinte technologique.
Le défi réside alors dans un équilibre entre cette intention et la
volonté de produire une installation ayant un fort potentiel expressif
d'un point de vue artistique. L'instrument doit ainsi être capable de
produire une variété de sons et d'images suffisante pour que
l'interacteur puisse participer à la cocréation des œuvres sans
ressentir, dans son utilisation, les contraintes techniques auxquelles a
répondu son développement.

Deux concepts sont notamment au cœur de cette recherche-création, le
premier est celui de la \emph{rétroaction}, issue de l'approche
cybernétique \citep{wiener_cybernetics_1948} qu'on définira ici
rapidement, pour y revenir plus tard, comme le phénomène par lequel, au
sein d'un système l'action d'un élément sur un autre entrainera une
réponse du second sur le premier. Le second concept est le principe de
\emph{concrétisation de l'objet technique} \citep{simondon_du_1958}.

Ce qui nous intéresse est la réflexion autour de la matérialisation sous
forme d'objet (dispositif ou instrument interactif) de la dimension
technique de notre idée initiale par le développement et la «
condensation » des fonctions des différents éléments~: « l'invention
apporte une vague de condensations, de concrétisations qui simplifient
l'objet en chargeant chaque structure d'une pluralité de fonctions»
\citep{simondon_imagination_2008}. Simondon explique ce processus en
invoquant le changement des composantes servant à dissiper la chaleur
issue du fonctionnement d'un moteur à combustion.

\begin{quote}
Ces ailettes de refroidissement, dans les premiers moteurs, sont comme
ajoutées de l'extérieur au cylindre et à la culasse théoriques,
géométriquement cylindriques ; elles ne remplissent qu'une seule
fonction, celle du refroidissement. Dans les moteurs récents, ces
ailettes jouent en plus un rôle mécanique, s'opposant comme des nervures
à une déformation de la culasse sous la poussée des gaz
\citep{simondon_du_1958}
\end{quote}

Cet exemple de concrétisation donne la direction de la méthodologie de
recherche-création que je mets sur pied. La réduction du nombre de
composantes et l'optimisation de leur rôle au sein de l'objet
constituent un axe structurant de ma démarche. Je propose donc de
réfléchir autour d'une problématique à laquelle je suis confronté
lorsque je crée des dispositifs en utilisant des technologies~: comment
concrétiser l'empreinte technologique et énergétique d'un instrument
interactif dont l'interaction révèle un ensemble de rétroactions
audiovisuelles? Cette recherche-création se structure autour de trois
axes soient:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  L'adoption d'une méthode de recherche-création cherchant à tendre vers
  la concrétisation de l'objet technique.
\item
  Le développement et la maîtrise d'un environnement de création
  permettant la réduction des ressources nécessaires pour produire
  l'instrument audiovisuel interactif.
\item
  La présentation sous forme performative d'une pièce sur l'instrument
  interactif où le registre expressif est rendu possible par la
  transformation de mouvements en sons, puis de ces sons qui deviennent
  simultanément des images en temps réel.
  \enlargethispage{\baselineskip}
\end{enumerate}

\hypertarget{premier-aperuxe7u-du-but-du-muxe9moire}{%
\section{Premier aperçu du but du
mémoire}\label{premier-aperuxe7u-du-but-du-muxe9moire}}

\emph{Électrocinétique} est le nom de la pièce alors
qu'\emph{Oscillographe} est le nom du logiciel régissant le comportement
de l'instrument interactif, l'instrument interactif est l'assemblage
formé par l'interface, les logiciels et le matériel permettant à
Électrocinétique d'être vu et entendu et de produire des expériences en
cocréation avec l'interacteur.

La pièce \emph{Électrocinétique, réinjection cinétique interactive}
s'inscrit dans une démarche de réduction d'empreinte qui implique une
maitrise du matériel et du logiciel. L'œuvre a été conçue en cherchant à
minimiser l'énergie électrique nécessaire à un déploiement de
performance audiovisuelle et en misant sur le développement de
composantes très peu énergivores alimentées grâce à des batteries
rechargeables. L'objectif était de rendre l'instrument interactif
autonome au niveau électrique et suffisamment portatif pour être
transporté dans un sac à dos et déployé de manière impromptue. À travers
ce processus de recherche-création, je mobilise l'objet familier du vélo
afin de le détourner pour produire une expérience «augmentée». Aussi,
l'expérience créée est de nature nomade et éphémère, l'instrument est
mon vélo et mon moyen de transport. L'objectif est alors d'offrir les
possibilités de réalisation de la performance suivante~:

\emph{Sur une place de nuit, un cycliste s'arrête devant les passants.
Après avoir retourné son vélo, il sort de son sac à dos un dispositif
qu'il installe en quelques minutes sans se brancher à quoi que ce soit
d'autre que le véhicule qu'il chevauchait encore il y a quelques
minutes. Certains passants s'arrêtent intrigués. Le jeune homme se met
alors à jouer avec le pédalier et à modifier la vitesse de rotation des
roues. Devant une foule de plus en plus nombreuse, des images prennent
forment projetées contre le mur qui borde la place tandis qu'une mélodie
électronique emplie l'espace d'une étrange composition musicale
interpellant quiconque veut bien ouvrir l'oreille. Après quelques
minutes de cet étrange spectacle, le cycliste range son appareil dans
son sac à dos, en moins de temps encore qu'il n'en a mis pour
l'installer. Il remonte sur son vélo puis s'en va, sans laisser de trace
physique.}

Le son d'\emph{Électrocinétique} est issu de rétroaction
(\emph{feedbacks}) dans une console de mixage audio. Pour produire cet
état fragile sur la console, je branche la sortie gauche et droite du
mélangeur dans les entrées gauche et droite correspondantes. En
changeant l'amplification et en manipulant les filtres de fréquences
intégrés, je crée le son tout en me laissant surprendre par son
caractère quelque peu imprévisible. J'enregistre des séances de
manipulations d'où j'isole certains segments audio que je juge
pertinents créant ainsi des échantillons qui feront partie de la
bibliothèque sonore de l'instrument.

Ces échantillons sonores sont joués dans le lecteur d'échantillons
d'Oscillographe. Ce lecteur d'échantillons a été programmé de façon à
pouvoir moduler la vitesse de lecture du son proportionnellement au
mouvement d'une roue de vélo. Cela a pour effet de pouvoir influencer la
hauteur du son tel un lecteur vinyle où l'on contrôlerait la vitesse de
rotation du disque. Plus il y a du mouvement, plus le son joue
rapidement. C'est une de ces possibilités qui est jouée devant public.

L'approche visuelle que je développe est en lien direct avec les outils
qui permettent de comprendre les signaux électriques. La dimension
visuelle d'Oscillographe est un oscilloscope qui compare l'amplitude du
signal sonore stéréo. La transformation du signal audio vers un signal
vidéo, effectuée par Oscillographe, est calquée sur les oscilloscopes
analogiques, plus particulièrement ceux qui permettent de comparer deux
signaux (X-Y) et qui peuvent servir de moniteur vectoriel. Contrairement
aux oscilloscopes X-Y conventionnels qui traduisent le son en image sur
un plan bidimensionnel, Oscillographe est construit dans un moteur de
rendu tridimensionnel et maintient la trace temporelle du signal dans
une forme 3d dynamique tout en maintenant le principe fondamental de
comparaison de signaux sur deux axes.

Les échantillons bruts, visualisés dans Oscillographe, créent un ballet
de lignes directement influencé par ce que l'on entend. La circulation
des signaux en temps réel, leur nature, leur organisation, leur phase et
leur chaos sont les matières premières d'Électrocinétique, le mouvement
qui active le dispositif.

À l'issue de cette première approche, nous avons une idée initiale du
projet et des questions qu'il soulève. Pour en rendre compte, nous
développerons d'abord les questions proposés par Simondon et Wiener
concernant l'objet technique, son évolution, son fonctionnement et les
paramètres internes de celui-ci dans le contexte d'un dispositif de
communication avec une entrée et une sortie. Pour donner suite à cette
réflexion technique, nous discuterons des implications esthétiques et
culturelles d'un tel travail. Il nous restera ensuite à expliquer en
détail l'élaboration de l'œuvre avant de faire un retour sur la
performance autant en termes de réception des spectateurs que dans une
initiative d'autocritique.
