\chapter{Cadrage esthétique et culturel}

\hypertarget{comparaison-avec-un-corpus-dux153uvres}{%
\section{Comparaison avec un corpus
d'œuvres}\label{comparaison-avec-un-corpus-dux153uvres}}

Je m'inspire du travail de nombreux artistes, et ce, autant sur les
plans conceptuels, visuels, sonores qu'en termes d'expérience proposée.
En gardant à l'esprit les concepts étudiés dans le chapitre précédent,
cette section présentera et expliquera comment certaines œuvres ont
influencé mon travail. Je choisis ici plutôt que d'avoir un rapport
analytique à ces œuvres, de dresser une liste structurée selon le type
d'inspiration associé à chacune des œuvres abordées. Cette constellation
permet d'éclairer les intuitions qui ont façonné
\emph{Électrocinétique}.

\hypertarget{influences-conceptuelles-dispositifs-critiques-pour-mouvements}{%
\subsection{Influences conceptuelles~: dispositifs critiques pour
mouvements}\label{influences-conceptuelles-dispositifs-critiques-pour-mouvements}}

En utilisant le mouvement pour interagir avec l'œuvre, je me suis
inspiré notamment du travail de l'artiste français Yann Toma. Il réalisa
l'installation \emph{Dynamo-Fukushima}
\citeyearpar{yann_toma_dynamo-fukushima_2011} dans laquelle l'effort de
plusieurs cyclistes permettait d'illuminer l'espace d'exposition du
Grand Palais à Paris. Cette œuvre propose une réflexion critique sur la
fragilité de l'existence plus précisément dans le contexte du rapport de
dépendance énergétique qui caractérise la vie contemporaine. En soutien
aux victimes de Fukushima, l'artiste a proposé aux participants de
prendre leurs responsabilités énergétiques en pédalant sur ces vélos. Le
discours est critique vis-à-vis l'usage du nucléaire, mais souhaite tout
de même mettre en place une atmosphère festive~:

Je me suis également inspiré par l'installation \emph{Champs de Pixels}
\citeyearpar{nova-lux_champs_2010} d'Érick Villeneuve et de Jean
Beaudoin. Présentée dans le Quartier des spectacles de Montréal, elle
était notamment activée et alimentée par les efforts des usagers sur des
Bixis stationnés. De cette pièce, que j'ai eu le plaisir d'expérimenter,
je retiens l'usage novateur de vélos libre-service présentés comme
interface active qui alimentait le résultat par l'effort collectif.

\emph{Agit P.O.V.} \citep{castongay_agit_2013} est un autre projet
destiné à démocratiser certaines notions d'électronique qui a retenu mon
attention. \emph{Agit P.O.V.} est un dispositif-œuvre qui incite les
gens à s'exprimer grâce à un module programmable lumineux installé sur
leur roue de vélo. Le processus de diffusion de l'œuvre inclut le
partage du savoir-faire par des ateliers où les participants assemblent
et apprennent à programmer l'objet. La documentation, le code et les
schémas électroniques de ce projet sont disponibles sur internet, ce qui
rend sa reproductibilité plus aisée. Cette perspective de
démocratisation des connaissances et d'expression artistique critique en
utilisant de la technologie a motivé l'adoption du Libre comme vecteur
de développement pour \emph{Electrocinétique}.

Concernant l'utilisation d'une roue comme objet artistique manipulable,
je me suis directement inspiré de Marcel Duchamps
\citeyearpar{duchamps_roue_1951} issue de sa série \emph{ready-made}.
Dans la filiation de l'idée de présenter un objet utilitaire comme
matériel artistique devenant une œuvre, je me suis intéressé à
construire une dynamique interactive qui transforme le comportement
attendu de l'objet en une expérience audiovisuelle à découvrir.

Sur le point de vue de la circularité entre les différents médiums, je
suis particulièrement fasciné par \emph{Very Nervous System}
\citep{rokeby_very_1986}. Dans cette œuvre, le mouvement est analysé par
un logiciel couplé à une caméra vidéo pointée sur une zone où un
performeur peut bouger. L'ensemble produit du son en fonction du
mouvement. Dans cette œuvre il se développe une relation d'écoute entre
le mouvement du corps et le son produit chez le performeur.

\hypertarget{influences-sonores-ruxe9troaction-comme-matiuxe8re-roue-comme-instrument}{%
\subsection{Influences sonores~: rétroaction comme matière, roue comme
instrument}\label{influences-sonores-ruxe9troaction-comme-matiuxe8re-roue-comme-instrument}}

C'est par le son que j'ai souhaité établir un lien corporel entre
l'usage et l'usager. La réponse du dispositif doit être instantanée et
expressive afin de révéler le potentiel de l'instrument sensible. Je
cherche à développer le plus possible l'instrument musical afin
d'obtenir une identification parfaite entre l'action et le résultat
sonore.

La relation entre le \emph{feedback} sonore et le mouvement trouve
également des échos dans différentes œuvres. Je pense notamment à la
pièce \emph{Pendulum Music} \citep{reich_pendulum_1968} où un microphone
est suspendu par son fil au-dessus d'un haut-parleur dans lequel il est
branché. Un mouvement de pendule est induit au microphone afin de le
faire passer momentanément à proximité du haut-parleur ce qui résulte en
un court moment d'accumulation de feedback. Le rythme qui émerge du
mouvement du micro et le travail de cette matière fragile qu'est le
feedback m'inspirent.

Dans la dynamique du travail sur le \emph{feedback}, je suis inspiré par
les timbres et le processus qui est à la genèse des pièces
\emph{Microphones} \citep{tudor_microphone_1973}. Travaillant
directement avec le feedback, l'auteur des pièces performe un dispositif
de filtres et de mix pendant de longues sessions. Le son se sculpte peu
à peu laissant émerger des moments complètement frénétiques entrecoupés
de courts silences un peu comme des respirations. La spécificité de la
réponse acoustique des enregistrements des pièces issus de ce processus
est directement liée à la résonance acoustique des lieux où sont
réalisées les séances de performances.

D'un point de vue rythmique, je souhaite établir une relation
symbiotique avec l'interaction tout en restant relativement minimaliste
et génératif. Inspiré par les propos recueillis dans \emph{L'art du
Trompe-l'Oreille Rythmique} \citep{feron_lart_2010}, je m'inspire du
travail sur la structure rythmique de Terry Riley et Steve Reich afin de
composer \emph{Électrocinétique}.

\hypertarget{influences-visuelles-illumination-et-animation-du-mouvement}{%
\subsection{Influences visuelles~: illumination et animation du
mouvement}\label{influences-visuelles-illumination-et-animation-du-mouvement}}

La dimension visuelle de mon projet s'articule principalement autour de
la visualisation du son produit par l'instrument. La représentation
fidèle du phénomène sonore et la rapidité d'affichage seront
privilégiées par rapport à la fidélité de reproduction du réel par
exemple.

Sur cet aspect, je m'inspire notamment du travail de Norman McLaren et
particulièrement de son film \emph{Synchromie}
\citeyearpar{mclaren_synchromy_1971}. Ce film explore la visualisation
de la partie de la pellicule qui est destinée à être entendue soit la
bande sonore optique. Les sons que l'on entend sont des interventions
faites sur la pellicule, presque exclusivement des rectangles de
différentes grosseurs à intervalles réguliers. L'aspect très épuré, le
contraste élevé des éléments vus et la correspondance directe avec ce
qui est entendu sont un exemple de synchronicité audiovisuelle
particulièrement probant.

Au point de vue de la programmation visuelle, j'ai eu recours à
plusieurs principes répertoriés dans \emph{The Nature of Code}
\citep{shiffman_nature_2012}, \emph{Form+Code in Design, Art, and
Architecture} \citep{lust_form+code_2010} ainsi que \emph{Learning
Processing} \citep{shiffman_learning_2015}. Ces ouvrages, accompagnés de
leurs exercices, m'ont permis d'explorer le registre expressif de la
visualisation graphique issue d'algorithmes. Afin de pouvoir traduire
les exemples traités dans ces livres vers le C++, j'ai approfondi ce
langage avec \emph{A Tour of C++} \citep{stroustrup_tour_2013},
\emph{The C++ Programming Language} \citep{stroustrup_c++_2013},
\emph{Programming~: principles and practice using C++}
\citep{stroustrup_programming_2014}. L'ensemble de ces documents a
contribué à saisir le fonctionnement et certaines particularités propres
au C++ notamment sa gestion très précise de la mémoire d'un logiciel. En
parallèle de l'apprentissage de ce langage, je me suis familiarisé avec
le fonctionnement d'\emph{openFrameworks} en me basant sur ces manuels,
\emph{openFrameworks Essentials} \citep{perevalov_openframeworks_2015},
\emph{Mastering openFrameworks~: Creative Coding Demystified}
\citep{perevalov_mastering_2013}, Programming Interactivity,
\citep{noble_programming_2012} ainsi que \emph{The Book of Shaders}
\citep{gonzalez_vivo_book_2015}. L'ensemble de ces lectures ont
contribué à transformer mon approche de résolution de problèmes
impliquant la programmation logicielle.

Dans la perspective de l'usage de la rétroaction pour générer un
résultat visuel, j'ai été très inspiré par le travail de Jean Pierre
Boyer et son instrument, le \emph{Boyétiseur}
\citeyearpar{boyer_lintegrale_1975}. Partant d'un phénomène observable,
soit la distorsion provoquée par la proximité d'un champ magnétique
adjacent au canon à électron dans un tube cathodique,
l'artiste-chercheur a construit un instrument permettant d'intervenir
sur ce phénomène. Autant par le résultat, que je trouve esthétiquement
et conceptuellement intéressant, que par la démarche de
recherche-création, le \emph{Boyétiseur} est intimement lié aux
processus d'\emph{Électrocinétique}.

\hypertarget{duxe9gagement-des-aspects-retenus-pour-lux153uvre}{%
\section{Dégagement des aspects retenus pour
l'œuvre}\label{duxe9gagement-des-aspects-retenus-pour-lux153uvre}}

Pour créer la partie installation d'Électrocinétique, je m'inspire du
travail à la fois critique et conceptuel d'artistes qui m'ont précédé et
qui ont pris comme sujet l'objet du vélo ainsi que le mouvement afin de
créer un objet qui s'active lorsqu'on lui transmet du mouvement.

Concernant le son produit, c'est le feedback audio qui demeure le
générateur de son le plus près du concept de réinjection. Deux modes
d'opération seront retenus pour l'instrument~: la lecture d'échantillons
où la vitesse est influencée par la manipulation de la roue ainsi que le
mode d'analyse en temps réel d'une source directe.

À propos de la dimension visuelle, le travail de musique pour
oscilloscope de Jerombeam Fenderson
\citeyearpar{fenderson_oscilloscope_2016} a également été une
inspiration importante. Son travail, à la fois visuel et sonore,
consiste à produire des sons stéréophoniques qui sont analysés par un
oscilloscope analogique. Sur son site se trouve une référence au
logiciel intitulé \emph{Oscilloscope} \cite{raber_oscilloscope_2018-1}.
Ce logiciel programmé en \emph{openFrameworks} permet de charger des
échantillons sonores et de les faire jouer en faisant fluctuer la
vitesse tout en affichant une représentation visuelle inspirée d'un
oscilloscope analogique.

Alors que j'expérimentais avec ce logiciel, en y mettant mes sons, je me
suis rendu compte de ses limites. Il dépendait notamment de plusieurs
librairies externes qui rendaient son portage vers le Raspberry Pi très
complexe. De là est née l'idée de la création d'un logiciel qui n'aurait
pas ces limitations au niveau du déploiement. \emph{Électrocinétique}
est basé sur le principe de visualisation de la stéréophonie sur l'axe
horizontal et vertical tout en tentant de limiter ses dépendances à des
librairies incompatibles avec l'architecture \emph{ARM}.

\hypertarget{perspective-communicationnelle-et-pertinence-dune-approche-de-recherche-cruxe9ation}{%
\section{Perspective communicationnelle et pertinence d'une approche de
recherche-création}\label{perspective-communicationnelle-et-pertinence-dune-approche-de-recherche-cruxe9ation}}

Mon instrument audiovisuel s'inscrit dans une vision particulière de la
notion d'instrument. Il est joué au sens de découverte ludique d'un
univers et au sens performatif du registre d'expression. J'inscris cette
démarche dans la continuité d'artistes pratiquant la recherche et la
création. J'ai compris ce processus lorsque cette recherche s'est
développée à travers les méthodes d'expression esthétique que j'ai
développées afin de favoriser l'émergence d'un registre. Cette curiosité
envers la matière sonore difficilement contrôlable qu'est le
\emph{feedback} m'amène à apprivoiser cet élément et à créer des
méthodes pour en extraire des expériences audiovisuelles.

Concernant le matériel, travailler avec des micro-ordinateurs
nécessitant beaucoup d'optimisations m'a forcé à mieux comprendre le
comportement des fonctions élémentaires de ces machines. La faible
capacité de calcul de ces machines offre un meilleur rendement
énergétique, mais implique de penser leurs relations avec les éléments
de l'ensemble de manière plus précise, de façon à sélectionner
uniquement les éléments indispensables.

Ces considérations pratiques et conceptuelles m'amènent à réfléchir sur
le processus de recherche-création d'instrument audiovisuel interactif,
ses composantes, son processus d'assemblage, l'expérience ainsi que son
registre d'interaction.
